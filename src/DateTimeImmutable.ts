import { DateTime, DateTimeUnit } from './DateTime';

/**
 * This class is exactly the same as {@link DateTime}
 */
export class DateTimeImmutable extends DateTime {
	startOf(unit?: DateTimeUnit, isStartOf: boolean = true): DateTime {
		return super.clone().startOf(unit, isStartOf);
	}

	// no need to override end of since it happens to use startOf.

	set(unit: DateTimeUnit, value: number): DateTime {
		return super.clone().set(unit, value);
	}

	add(value: number, unit: DateTimeUnit): DateTime {
		return super.clone().add(value, unit);
	}

	setUnixTimestamp(stamp: number) {
		return super.clone().setUnixTimestamp(stamp);
	}

	setYear(y: number): DateTime {
		return super.clone().setYear(y);
	}

	setMonth(m: number): DateTime {
		return super.clone().setMonth(m);
	}

	// no need to change setWeekday

	setDay(d: number): DateTime {
		return super.clone().setDay(d);
	}

	setHour(h: number): DateTime {
		return super.clone().setHour(h);
	}

	setMinute(m: number): DateTime {
		return super.clone().setMinute(m);
	}

	setSecond(s: number): DateTime {
		return super.clone().setSecond(s);
	}

	setMillisecond(ms: number): DateTime {
		return super.clone().setMillisecond(ms);
	}
}