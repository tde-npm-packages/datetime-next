export interface TimeInterface {
	hour: number;
	minute: number;
	second: number;
}