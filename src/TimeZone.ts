/**
 * TimeZone helper class, as slim as possible
 *
 * Lots of code borrowed from [Luxon](https://github.com/moment/luxon)
 */
import { DateTime } from './DateTime';

export class TimeZone {
	static dtfCache = {};

	static locale = 'en-US';
	static usRegex = /(\d+).(\d+).(\d+),?\s+(\d+).(\d+)(.(\d+))?/;

	static formatOptions = {
		timeZone: 'UTC',
		hourCycle: 'h23',
		year: 'numeric',
		month: 'numeric',
		day: 'numeric',
		hour: 'numeric',
		minute: 'numeric'
	};

	/** @ignore */
	static makeDTF(zone: string) {
		if (!TimeZone.dtfCache[zone]) {
			const options = Object.assign({}, TimeZone.formatOptions);
			options.timeZone = zone;
			TimeZone.dtfCache[zone] = new Intl.DateTimeFormat(TimeZone.locale, options as any);
		}

		return TimeZone.dtfCache[zone];
	}

	/**
	 * Checks if IANA timezone is valid
	 *
	 * @param zone zone name ex 'America/Toronto'
	 */
	static isZoneValid(zone: string) {
		try {
			new Intl.DateTimeFormat('en-US', { timeZone: zone }).format();
			return true;
		}
		catch (e) {
			return false;
		}
	}

	/**
	 * Parse GTM offset such as 'GMT+4:30' to minute offset from UTC
	 *
	 * @param specifier
	 */
	static parseGMTOffset(specifier: string) {
		const match = specifier.match(/^GMT(0|[+-]\d{1,2})?:?(\d{1,2})?$/i);
		if (!match[1]) {
			return 0;
		}

		if (match) {
			return -60 * +match[1] + (match[2] ? -match[2] : 0);
		}
	}

	static parseDate(dateString: string) {
		dateString = dateString.replace(/[\u200E\u200F]/g, '');
		return [].slice.call(TimeZone.usRegex.exec(dateString), 1).map(Math.floor);
	}

	/**
	 * Calculate difference in parsed datetimes
	 *
	 * @ignore
	 * @param d1
	 * @param d2
	 */
	static diffMinutes(d1: number[], d2: number[]) {
		let day = d1[1] - d2[1];
		const hour = d1[3] - d2[3];
		const min = d1[4] - d2[4];

		if (day > 15) {
			day = -1;
		}
		if (day < -15) {
			day = 1;
		}

		return 60 * (24 * day + hour) + min;
	}

	/**
	 * Calculate timezone offset for IANA timezone
	 *
	 * @param timeZone zone name ex 'America/Toronto'
	 * @param date optional date to calculate offset (for DST) otherwise current date is taken
	 */
	static offset(timeZone: string, date?: Date | DateTime) {
		const dtf = TimeZone.makeDTF(timeZone);
		const utcDtf = TimeZone.makeDTF('UTC');

		if (!date) {
			date = new Date();
		}
		if (date instanceof DateTime) {
			date = date.date;
		}

		return TimeZone.diffMinutes(
			TimeZone.parseDate(utcDtf.format(date)),
			TimeZone.parseDate(dtf.format(date))
		);
	}
}