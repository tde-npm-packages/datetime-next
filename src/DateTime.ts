import { TimeZone }      from './TimeZone';
import { DateInterface } from './Interface/DateInterface';
import { TimeInterface } from './Interface/TimeInterface';

const absFloor = n => (n < 0 ? Math.ceil(n) || 0 : Math.floor(n));

export const UNIT_MILLISECOND = 'millisecond';
export const UNIT_SECOND = 'second';
export const UNIT_MINUTE = 'minute';
export const UNIT_HOUR = 'hour';
export const UNIT_DAY = 'day';
export const UNIT_WEEK = 'week';
export const UNIT_MONTH = 'month';
export const UNIT_QUARTER = 'quarter';
export const UNIT_YEAR = 'year';

/**
 * DateTime units
 *
 * @typedef DateTimeUnit
 */
export type DateTimeUnit =
	'millisecond'
	| 'second'
	| 'minute'
	| 'hour'
	| 'day'
	| 'week'
	| 'month'
	| 'quarter'
	| 'year';

export interface DateTimeMonthLocale {
	short: string;
	long: string;
	narrow: string;
}

/**
 * Next generation super slim Date wrapper
 *
 * basic usage:
 * ```ts
 * const dt = new DateTime('2021-06-12');
 * ```
 */
export class DateTime implements DateInterface, TimeInterface {

	/** @ignore */
	static SECONDS_PER_MINUTE = 60;
	/** @ignore */
	static SECONDS_PER_HOUR = 3600;
	/** @ignore */
	static SECONDS_PER_DAY = 24 * DateTime.SECONDS_PER_HOUR;
	/** @ignore */
	static SECONDS_PER_WEEK = 7 * DateTime.SECONDS_PER_DAY;
	/** @ignore */
	static MILLISECONDS_PER_SECOND = 1000;
	/** @ignore */
	static MILLISECONDS_PER_MINUTE = DateTime.SECONDS_PER_MINUTE * DateTime.MILLISECONDS_PER_SECOND;
	/** @ignore */
	static MILLISECONDS_PER_HOUR = DateTime.SECONDS_PER_HOUR * DateTime.MILLISECONDS_PER_SECOND;
	/** @ignore */
	static MILLISECONDS_PER_DAY = DateTime.SECONDS_PER_DAY * DateTime.MILLISECONDS_PER_SECOND;
	/** @ignore */
	static MILLISECONDS_PER_WEEK = DateTime.SECONDS_PER_WEEK * DateTime.MILLISECONDS_PER_SECOND;

	static TIME_FORMAT = 'HH:mm:ss';
	static TIME_FORMAT_SHORT = 'HH:mm';
	static PRECISE_TIME_FORMAT = 'HH:mm:ss.SSS';
	static DATE_FORMAT = 'YYYY-MM-DD';
	static DATE_TIME_FORMAT: string = DateTime.DATE_FORMAT + ' ' + DateTime.TIME_FORMAT;
	static DATE_TIME_FORMAT_SHORT: string = DateTime.DATE_FORMAT + ' ' + DateTime.TIME_FORMAT_SHORT;
	static PRECISE_DATE_TIME_FORMAT: string = DateTime.DATE_FORMAT + ' ' + DateTime.PRECISE_TIME_FORMAT;

	/** @ignore */
	static REGEX_PARSE = /^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[^0-9]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/;
	/** @ignore */
	static REGEX_PARSE_TZ = /^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[^0-9]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?([+-])?(\d{2})?:?(\d{2})?$/;
	/** @ignore */
	static REGEX_FORMAT = /\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g;

	/** @ignore */
	static INVALID_DATE_STRING = 'Invalid Date';

	/** @ignore */
	static defaultLocale = new Intl.DateTimeFormat().resolvedOptions();
	/** @ignore */
	static defaultLocaleName = DateTime.defaultLocale.locale;
	/** @ignore */
	static defaultTimezone = DateTime.defaultLocale.timeZone;

	/** @ignore */
	static monthNames: DateTimeMonthLocale[] = [];
	/** @ignore */
	static monthNamesOverride: DateTimeMonthLocale[] = [];

	/** @ignore */
	static weekNames: DateTimeMonthLocale[] = [];
	/** @ignore */
	static weekNamesOverride: DateTimeMonthLocale[] = [];

	date: Date;
	locale = DateTime.defaultLocaleName;
	timeZone = DateTime.defaultTimezone;

	// Timezone offset in minutes from UTC
	localOffset = 0;

	// @formatter:off
	get timeString() { return this.getTimeString(); }
	get dateString() { return this.getDateString(); }
	get localString() { return this.getLocalString(); }
	get localStringShort() { return this.getLocalStringShort(); }
	get isoString() { return this.getIsoString(); }
	get isoWeekday() { return this.getIsoWeekday(); }
	get preciseTimestamp() { return this.getPreciseTimestamp(); }
	get unixTimestamp() { return this.getUnixTimestamp(); }
	get millisecond() { return this.getMillisecond(); }
	get second() { return this.getSecond(); }
	get minute() { return this.getMinute(); }
	get hour() { return this.getHour(); }
	get day() { return this.getDay(); }
	get weekday() { return this.getWeekday(); }
	get month() { return this.getMonth(); }
	get year() { return this.getYear(); }
	get monthName() { return this.getMonthName(); }
	// @formatter:on

	/**
	 * Constructs a `DateTime` object based on provided data, input options:
	 *
	 * * `number` - milliseconds from unix epoch
	 * * `string` - ISO datetime format: `2020-09-23T17:23:11+04:30` also accepts partial data (only year is required)
	 * * `Date` - Javascript `Date` object
	 * * `DateTime` - another instance of `DateTime` (clone will be performed)
	 * @param input
	 */
	constructor(input?: number | string | Date | DateTime) {
		if (typeof input === 'number') {
			this.date = new Date(input);
		}
		else if (typeof input === 'string') {
			this.date = DateTime.parseDate(input);
		}
		else if (input instanceof Date) {
			this.date = input;
		}
		else if (input instanceof DateTime) {
			this.date = input.clone().date;
		}
		else {
			this.date = new Date();
		}
	}

	/**
	 * Parse datetime from a string into javascript Date object.
	 *
	 * @param date
	 */
	static parseDate(date: string) {
		if (/Z$/i.test(date)) {
			return new Date(date);
		}

		const d = date.match(DateTime.REGEX_PARSE_TZ);
		if (d) {
			const year = +d[1];
			const month = +d[2] - 1 || 0;
			if (month > 11) {
				return new Date(undefined); // Invalid date
			}

			const day = +d[3] || 1;
			if (day > 31) {
				return new Date(undefined); // Invalid date
			}
			const hour = +d[4] || 0;
			if (hour > 24) {
				return new Date(undefined); // Invalid date
			}
			const minute = +d[5] || 0;
			if (minute > 59) {
				return new Date(undefined); // Invalid date
			}
			const second = +d[6] || 0;
			if (second > 59) {
				return new Date(undefined); // Invalid date
			}

			// this cannot fail as regex would not catch anything else than 000-999
			const millisecond = +(d[7] || '0').substring(0, 3);

			const offsetMultiplier = d[8] === '+' ? 1 : -1;
			const hourOffset = +(d[9] || '0');
			const minuteOffset = +(d[10] || '0');
			const offset = hourOffset * 60 + minuteOffset

			const date = new Date(Date.UTC(year, month, day, hour, minute, second, millisecond));
			if (offset !== 0) {
				date.setUTCMilliseconds(
					date.getUTCMilliseconds() - offset * DateTime.MILLISECONDS_PER_MINUTE * offsetMultiplier
				)
			}

			return date;
		}

		return new Date(undefined);
	}

	static monthDiff(a: DateTime, b: DateTime) {
		// function from moment.js in order to keep the same result
		if (a < b) {
			return -DateTime.monthDiff(b, a);
		}

		const wholeMonthDiff = ((b.getYear() - a.getYear()) * 12) + (b.getMonth() - a.getMonth());

		const anchor = a.clone().addMonth(wholeMonthDiff);
		const c = b.valueOf() - anchor.valueOf() < 0;
		const anchor2 = a.clone().addMonth(wholeMonthDiff + (c ? -1 : 1));

		return +(-(wholeMonthDiff +
			(
				(b.valueOf() - anchor.valueOf()) /
				(c ? (anchor.valueOf() - anchor2.valueOf()) : (anchor2.valueOf() - anchor.valueOf()))
			)
		) || 0);
	}

	/**
	 * Special constructors:
	 */
	static fromPreciseTimestamp(timestampMilliseconds: number): DateTime {
		return new DateTime(timestampMilliseconds);
	}

	static fromUnixTimestamp(timestamp: number): DateTime {
		return new DateTime(timestamp * 1000);
	}

	static fromUtcString(input: string, format: string = DateTime.DATE_TIME_FORMAT): DateTime {
		return new DateTime(input);
	}

	static fromUtcPreciseString(input: string, format: string = DateTime.PRECISE_DATE_TIME_FORMAT): DateTime {
		return new DateTime(input);
	}

	/**
	 * TODO: fix this
	 *
	 * @param input
	 * @param format
	 */
	static fromLocalPreciseString(input: string, format: string = DateTime.PRECISE_DATE_TIME_FORMAT): DateTime {
		return new DateTime(input);
	}

	/**
	 * Gets current date and time with specified format
	 *
	 * @param format default `YYYY-MM-DD HH:mm:ss`
	 */
	static getNowString(format: string = DateTime.DATE_TIME_FORMAT): string {
		return (new DateTime()).getString(format);
	}

	/**
	 * Gets formatted date and time with milliseconds precision in format `YYYY-MM-DD HH:mm:ss.SSS`
	 */
	static getPreciseNowString(): string {
		return (new DateTime()).getString(DateTime.PRECISE_DATE_TIME_FORMAT);
	}

	/**
	 * Gets JS timestamp (milliseconds precision) of current date and time
	 */
	static getPreciseTimestamp(): number {
		return (new DateTime()).getPreciseTimestamp();
	}

	/**
	 * Gets unix timestamp (seconds precision) of current date and time
	 */
	static getUnixTimestamp(): number {
		return (new DateTime()).getUnixTimestamp();
	}

	static isValid(s: string, format: string = DateTime.DATE_TIME_FORMAT, strict = false) {
		return (new DateTime(s)).isValid();
	}

	/**
	 * Changes global locale clearing data such as month names
	 *
	 * @param locale
	 */
	static setDefaultLocale(locale: string) {
		if (DateTime.defaultLocaleName === locale) {
			return;
		}

		DateTime.defaultLocaleName = locale;
		DateTime.monthNames = [];
		DateTime.weekNames = [];
	}

	/**
	 * Override the default timezone globally if you wish to use different timezone across all application
	 *
	 * @param timezone IANA timezone string
	 */
	static setDefaultTimezone(timezone: string) {
		if (DateTime.defaultTimezone === timezone) {
			return;
		}

		DateTime.defaultTimezone = timezone;
	}

	/**
	 * Overrides global month names
	 *
	 * @param names
	 */
	static setMonthNamesOverride(names: DateTimeMonthLocale[]) {
		DateTime.monthNamesOverride = names;
	}

	/**
	 * Clears overridden month names
	 */
	static clearMonthNamesOverride() {
		DateTime.monthNamesOverride = [];
	}

	/**
	 * Overrides global week names
	 *
	 * @param names
	 */
	static setWeekNamesOverride(names: DateTimeMonthLocale[]) {
		DateTime.weekNamesOverride = names;
	}

	/**
	 * Clears overridden week names
	 */
	static clearWeekNamesOverride() {
		DateTime.weekNamesOverride = [];
	}

	/**
	 * Get array of month names, keep it in cache
	 *
	 * @param type
	 * @param locale
	 */
	static getMonthNames(type: 'short' | 'long' | 'narrow' = 'long', locale = DateTime.defaultLocaleName): string[] {
		if (DateTime.monthNamesOverride.length > 0) {
			return DateTime.monthNamesOverride.map(m => m[type]);
		}

		if (DateTime.monthNames.length > 0) {
			return DateTime.monthNames.map(m => m[type]);
		}

		// don't have this yet, time to build
		const longFormat = new Intl.DateTimeFormat(locale, { month: 'long' }).format;
		const shortFormat = new Intl.DateTimeFormat(locale, { month: 'short' }).format;
		const narrowFormat = new Intl.DateTimeFormat(locale, { month: 'narrow' }).format;

		const monthNames: DateTimeMonthLocale[] = [];
		for (let i = 0; i < 12; i++) {
			const d = new Date(Date.UTC(2021, i));
			monthNames.push({
				long: longFormat(d),
				short: shortFormat(d),
				narrow: narrowFormat(d)
			});
		}

		DateTime.monthNames = monthNames;

		return DateTime.monthNames.map(m => m[type]);
	}

	/**
	 * Get month name: 1 based (1 = January)
	 *
	 * @param m month number (not index)
	 * @param type
	 */
	static getMonthName(m: number, type: 'short' | 'long' | 'narrow' = 'long') {
		const monthNames = DateTime.getMonthNames(type);
		return monthNames[m - 1];
	}

	/**
	 * Get array of week names, keep it in cache
	 *
	 * @param type
	 * @param locale
	 */
	static getWeekNames(type: 'short' | 'long' | 'narrow' = 'long', locale = DateTime.defaultLocaleName): string[] {
		if (DateTime.weekNamesOverride.length > 0) {
			return DateTime.weekNamesOverride.map(m => m[type]);
		}

		if (DateTime.weekNames.length > 0) {
			return DateTime.weekNames.map(m => m[type]);
		}

		// don't have this yet, time to build
		const longFormat = new Intl.DateTimeFormat(locale, { weekday: 'long' }).format;
		const shortFormat = new Intl.DateTimeFormat(locale, { weekday: 'short' }).format;
		const narrowFormat = new Intl.DateTimeFormat(locale, { weekday: 'narrow' }).format;

		const weekNames: DateTimeMonthLocale[] = [];
		for (let i = 7; i < 14; i++) {
			const d = new Date(Date.UTC(2021, 5, i));
			weekNames.push({
				long: longFormat(d),
				short: shortFormat(d),
				narrow: narrowFormat(d)
			});
		}

		DateTime.weekNames = weekNames;

		return DateTime.weekNames.map(m => m[type]);
	}

	/**
	 * Get iso week name: 1 based (1 = Monday)
	 *
	 * @param m week number (not index)
	 * @param type
	 */
	static getWeekName(m: number, type: 'short' | 'long' | 'narrow' = 'long') {
		const weekNames = DateTime.getWeekNames(type);
		return weekNames[m - 1];
	}

	/**
	 * Checks if date is the same or is {@link DateTimeUnit} of is the same
	 *
	 * @param other
	 * @param unit {@link DateTimeUnit}
	 */
	isSame(other: DateTime, unit?: DateTimeUnit) {
		return this.startOf(unit) <= other && other <= this.endOf(unit);
	}

	isAfter(other: DateTime, unit?: DateTimeUnit) {
		return other < this.startOf(unit);
	}

	isSameOrAfter(other: DateTime, unit?: DateTimeUnit) {
		return other <= this.startOf(unit);
	}

	isBefore(that: DateTime, unit?: DateTimeUnit) {
		return this.endOf(unit) < that;
	}

	isSameOrBefore(that: DateTime, unit?: DateTimeUnit) {
		return this.endOf(unit) <= that;
	}

	isValid() {
		return !(this.date.toString() === DateTime.INVALID_DATE_STRING);
	}

	valueOf() {
		return this.date.valueOf() + this.localOffset * DateTime.MILLISECONDS_PER_HOUR;
	}

	daysInMonth() {
		const d = new Date(this.date.getFullYear(), this.getMonth(), 0);
		return d.getDate();
	}

	/**
	 * Sets date time to a start of specific period
	 *
	 * @param unit
	 * @param isStartOf if this is set to false, function will return the end of period
	 */
	startOf(unit?: DateTimeUnit, isStartOf = true) {
		switch (unit) {
			case UNIT_YEAR:
				return this
					.setMonth(isStartOf ? 1 : 12)
					.setDay(isStartOf ? 1 : this.daysInMonth())
					.setTimeTo(isStartOf);
			case UNIT_MONTH:
				return this
					.setDay(isStartOf ? 1 : this.daysInMonth())
					.setTimeTo(isStartOf);
			case UNIT_WEEK:
				return this
					.setWeekday(isStartOf ? 1 : 7)
					.setTimeTo(isStartOf);
			case UNIT_DAY:
				return this
					.setTimeTo(isStartOf);
			case UNIT_HOUR:
				return this
					.setMinute(isStartOf ? 0 : 59)
					.setSecond(isStartOf ? 0 : 59)
					.setMillisecond(isStartOf ? 0 : 999);
			case UNIT_MINUTE:
				return this
					.setSecond(isStartOf ? 0 : 59)
					.setMillisecond(isStartOf ? 0 : 999);
			case UNIT_SECOND:
				return this.setMillisecond(isStartOf ? 0 : 999);
			default:
				return this;
		}
	}

	endOf(unit: DateTimeUnit) {
		return this.startOf(unit, false);
	}

	set(unit: DateTimeUnit, value: number) {
		switch (unit) {
			case UNIT_YEAR:
				return this.setYear(value);
			case UNIT_MONTH:
				return this.setMonth(value);
			case UNIT_DAY:
				return this.setDay(value);
			case UNIT_WEEK:
				return this.setDay(Math.round(value * 7));
			case UNIT_MINUTE:
				return this.setPreciseTimestamp(Math.round(value * DateTime.MILLISECONDS_PER_MINUTE));
			case UNIT_HOUR:
				return this.setPreciseTimestamp(Math.round(value * DateTime.MILLISECONDS_PER_HOUR));
			case UNIT_SECOND:
				return this.setPreciseTimestamp(Math.round(value * DateTime.MILLISECONDS_PER_SECOND));
			case UNIT_MILLISECOND:
				return this.setPreciseTimestamp(value);
			default:
				throw Error('Unrecognized unit');
		}
	}

	get(unit: DateTimeUnit) {
		switch (unit) {
			case UNIT_YEAR:
				return this.getYear();
			case UNIT_MONTH:
				return this.getMonth();
			case UNIT_DAY:
				return this.getDay();
			case UNIT_WEEK:
				return this.getWeekday();
			case UNIT_MINUTE:
				return this.getMinute();
			case UNIT_HOUR:
				return this.getHour();
			case UNIT_SECOND:
				return this.getSecond();
			case UNIT_MILLISECOND:
				return this.getMillisecond();
			default:
				throw Error('Unrecognized unit');
		}
	}

	add(value: number, unit: DateTimeUnit) {
		switch (unit) {
			case UNIT_YEAR:
				return this.addYear(value);
			case UNIT_MONTH:
				return this.addMonth(value);
			case UNIT_DAY:
				return this.addDay(value);
			case UNIT_WEEK:
				return this.addWeek(value);
			case UNIT_MINUTE:
				return this.addMinute(value);
			case UNIT_HOUR:
				return this.addHour(value);
			case UNIT_SECOND:
				return this.addSecond(value);
			case UNIT_MILLISECOND:
				return this.addMillisecond(value);
			default:
				throw Error('Unrecognized unit');
		}
	}

	subtract(number: number, string) {
		return this.add(number * -1, string);
	}

	diff(that: DateTime, unit?: DateTimeUnit, asFloat = false) {
		const diff = this.valueOf() - that.valueOf();
		let monthDiff = DateTime.monthDiff(this, that);

		if (!unit) {
			return diff;
		}

		let result;

		// @formatter:off
		switch (unit) {
			case UNIT_YEAR: result = monthDiff / 12; break;
			case UNIT_MONTH: result = monthDiff; break;
			case UNIT_QUARTER: result = monthDiff / 3; break;
			case UNIT_WEEK: result = diff / DateTime.MILLISECONDS_PER_WEEK; break;
			case UNIT_DAY: result = diff / DateTime.MILLISECONDS_PER_DAY; break;
			case UNIT_HOUR: result = diff / DateTime.MILLISECONDS_PER_HOUR; break;
			case UNIT_MINUTE: result = diff / DateTime.MILLISECONDS_PER_MINUTE; break;
			case UNIT_SECOND: result = diff / DateTime.MILLISECONDS_PER_SECOND; break;
			default: result = diff;
		}
		// @formatter:on

		return asFloat ? result : absFloor(result);
	}

	clone() {
		return new DateTime(this.date.valueOf());
	}

	toDate() {
		return this.date;
	}

	toJSON() {
		return this.isValid() ? this.toISOString() : null;
	}

	toISOString() {
		return this.date.toISOString();
	}

	toString() {
		return this.date.toUTCString();
	}

	stripDate(): DateInterface {
		return {
			year: this.year,
			month: this.month,
			day: this.day
		};
	}

	stripTime(): TimeInterface {
		return {
			hour: this.hour,
			minute: this.minute,
			second: this.second
		};
	}

	/**
	 * Returns unix timestamp with milliseconds precision
	 */
	getPreciseTimestamp(): number {
		return this.date.getTime();
	}

	/**
	 * Sets the actual value of date object
	 *
	 * @param unix timestamp in milliseconds
	 */
	setPreciseTimestamp(unix: number): DateTime {
		this.date.setTime(unix);

		return this;
	}

	/**
	 * Returns unix timestamp with seconds precision
	 */
	getUnixTimestamp() {
		return Math.floor(this.valueOf() / 1000);
	}

	setUnixTimestamp(stamp: number): DateTime {
		this.date.setTime(Math.floor(stamp / 1000));

		return this;
	}

	getIsoString(): string {
		return this.date.toISOString();
	}

	getIsoWeekday(): number {
		const weekday = this.date.getDay();
		return 1 + (weekday + 6) % 7;
	}

	/**
	 * Gets a formatted date time string
	 *
	 * Format properties:
	 *
	 * | Symbol | Description | Example |
	 * | :------ | :------ | :------ |
	 * | YY | Year in 2 digit format | 21 |
	 * | YYYY | Year in full format | 2021 |
	 * | M | Month digit not padded with zeros | 4 |
	 * | MM | Month padded to 2 digits | 04 |
	 * | MMM | Short locale aware month name | Dec |
	 * | MMMM | Long locale aware month name | December |
	 * | d | Weekday number | 5 |
	 * | dd | Weekday locale aware narrow name | F |
	 * | ddd | Weekday locale aware short name | Fri |
	 * | dddd | Weekday locale aware short name | Friday |
	 * | D | Day of month not padded | 9 |
	 * | DD | Day of month padded to 2 digits | 09 |
	 * | H | Hour in 24h format not padded | 7 |
	 * | HH | Hour in 24h format padded to 2 digits | 07 |
	 * | h | Hour in 12h format not padded | 7 |
	 * | hh | Hour in 12h format padded to 2 digits | 07 |
	 * | a | Meridian locale aware in lowercase | am |
	 * | A | Meridian locale aware in uppercase | AM |
	 * | m | Minute not padded | 5 |
	 * | mm | Minute padded to 2 digits | 05 |
	 * | s | Second not padded | 3 |
	 * | ss | Second padded to 2 digits | 03 |
	 * | SSS | Milliseconds padded to 3 digits | 245 |
	 * | Z | Timezone offset from UTC with colon | +02:00 |
	 * | ZZ | Timezone offset from UTC without colon | +02:00 |
	 *
	 * @param format format to convert to
	 * @param local if this is set to true, current timezone offset will be used to format date to local time
	 */
	getString(format: string = DateTime.DATE_TIME_FORMAT, local = false): string {
		if (!this.isValid()) {
			return DateTime.INVALID_DATE_STRING;
		}

		if (local) {
			// This should be working properly as we are shifting time by timezone offset
			// normally it would be problematic because of DST however timezone offset is taken from current time
			// not from shifted time
			return new DateTime(
				this.valueOf() - this.getTimezoneOffset() * DateTime.MILLISECONDS_PER_MINUTE
			).getString(format);
		}

		return format.replace(
			DateTime.REGEX_FORMAT,
			(match: string, nonMatch) => {
				if (nonMatch) {
					return nonMatch;
				}

				// This should not be in preformatted array
				switch (match) {
					// @formatter:off
					case 'YY': return this.getYear().toString().slice(-2);
					case 'YYYY': return this.getYear();
					case 'M': return this.getMonth();
					case 'MM': return this.getMonth().toString().padStart(2, '0');
					case 'MMM': return this.getMonthName('short');
					case 'MMMM': return this.getMonthName();
					case 'D': return this.getDay();
					case 'DD': return this.getDay().toString().padStart(2, '0');
					case 'd': return this.getWeekday().toString();
					case 'dd': return this.getWeekdayName('narrow');
					case 'ddd': return this.getWeekdayName('short');
					case 'dddd': return this.getWeekdayName('long');
					case 'H': return this.getHour();
					case 'HH': return this.getHour().toString().padStart(2, '0');
					case 'h': return (this.getHour() % 12 || 12).toString();
					case 'hh': return (this.getHour() % 12 || 12).toString().padStart(2, '0');
					case 'a': return this.getMeridian().toLowerCase();
					case 'A': return this.getMeridian();
					case 'm': return this.getMinute().toString();
					case 'mm': return this.getMinute().toString().padStart(2, '0');
					case 's': return this.getSecond().toString();
					case 'ss': return this.getSecond().toString().padStart(2, '0');
					case 'SSS': return this.getMillisecond().toString().padStart(3, '0');
					case 'Z': return this.getTimezoneOffsetString();
					case 'ZZ': return this.getTimezoneOffsetString(false);
					default: return '';
					// @formatter:on
				}
			}
		); // 'ZZ'
	}

	getPreciseString(format: string = DateTime.PRECISE_DATE_TIME_FORMAT): string {
		return this.getString(format);
	}

	getTimeString(format: string = DateTime.TIME_FORMAT): string {
		return this.getString(format);
	}

	getDateString(format: string = DateTime.DATE_FORMAT): string {
		return this.getString(format);
	}

	getLocalString(format: string = DateTime.DATE_TIME_FORMAT): string {
		return this.getString(format, true);
	}

	getLocalStringShort(format: string = DateTime.DATE_TIME_FORMAT_SHORT): string {
		return this.getString(format, true);
	}

	getLocalPreciseString(format: string = DateTime.PRECISE_DATE_TIME_FORMAT): string {
		return this.getString(format, true);
	}

	getDateObject() {
		return this.date;
	}

	/**
	 * Get year in UTC
	 */
	getYear(): number {
		return this.date.getUTCFullYear();
	}

	/**
	 * Get month in UTC
	 */
	getMonth(): number {
		return this.date.getUTCMonth() + 1;
	}

	/**
	 * Get month name in current locale
	 * @param type
	 */
	getMonthName(type: 'long' | 'short' | 'narrow' = 'long'): string {
		return new Intl
			.DateTimeFormat(this.locale, { month: type })
			.format(this.date);
	}

	/**
	 * Get day in UTC
	 */
	getDay(): number {
		return this.date.getUTCDate();
	}

	/**
	 * Get weekday in UTC
	 */
	getWeekday(): number {
		return this.getIsoWeekday();
	}

	/**
	 * Get weekday name in current locale
	 * @param type
	 */
	getWeekdayName(type: 'long' | 'short' | 'narrow' = 'long') {
		const parts = new Intl
			.DateTimeFormat(this.locale, { weekday: type })
			.formatToParts(this.date);

		return parts.find(p => p.type === 'weekday').value;
	}

	/**
	 * Get Hour in UTC
	 */
	getHour(): number {
		return this.date.getUTCHours();
	}

	/**
	 * Get meridian (am/pm) in current locale
	 */
	getMeridian() {
		const parts = new Intl
			.DateTimeFormat(this.locale, { timeStyle: 'short', hour12: true } as any)
			.formatToParts(this.date)
		;
		return parts.find(p => p.type === 'dayPeriod').value;
	}

	/**
	 * Get minute in UTC
	 */
	getMinute(): number {
		return this.date.getUTCMinutes();
	}

	/**
	 * Get second in UTC
	 */
	getSecond(): number {
		return this.date.getUTCSeconds();
	}

	/**
	 * Get milliseconds in UTC
	 */
	getMillisecond(): number {
		return this.date.getUTCMilliseconds();
	}

	/**
	 * Get timezone offset in minutes from UTC, returns the number of minutes from utc ex -120
	 *
	 * @param timeZone IANA string for timezone
	 */
	getTimezoneOffset(timeZone?: string): number {
		if (!timeZone) {
			timeZone = this.timeZone;
		}

		return TimeZone.offset(timeZone, this.date);
	}

	/**
	 * Gets timezone offset string in hours from UTC, ex `+0200` or if withColon specified: `+02:00`
	 *
	 * @param withColon
	 * @param timeZone
	 */
	getTimezoneOffsetString(withColon = true, timeZone?: string) {
		let offset = this.getTimezoneOffset(timeZone);
		const sign = offset < 0 ? '-' : '+';
		offset = Math.abs(offset);
		const hours = Math.floor(offset / 60).toString().padStart(2, '0');
		const minutes = Math.floor(offset % 60).toString().padStart(2, '0');

		return sign + hours + (withColon ? ':' : '') + minutes;
	}

	/**
	 * Sets the UTC Year
	 *
	 * @category DateTime Manipulation - Setters
	 */
	setYear(y: number): DateTime {
		this.date.setUTCFullYear(y);
		return this;
	}

	/**
	 * Sets the UTC Month
	 *
	 * @category DateTime Manipulation - Setters
	 */
	setMonth(m: number): DateTime {
		this.date.setUTCMonth(m - 1);
		return this;
	}

	/**
	 * Sets the UTC weekday
	 *
	 * @category DateTime Manipulation - Setters
	 */
	setWeekday(week: number): DateTime {
		const isoWeekday = this.getWeekday();
		const diff = week - isoWeekday;

		this.setDay(this.getDay() + diff);

		return this;
	}

	/**
	 * Sets the UTC day of month (not weekday)
	 *
	 * @category DateTime Manipulation - Setters
	 */
	setDay(d: number): DateTime {
		this.date.setUTCDate(d);
		return this;
	}

	/**
	 * Sets the UTC hour
	 *
	 * @category DateTime Manipulation - Setters
	 */
	setHour(h: number): DateTime {
		this.date.setUTCHours(h);
		return this;
	}

	/**
	 * Sets the UTC minute
	 *
	 * @category DateTime Manipulation - Setters
	 */
	setMinute(m: number): DateTime {
		this.date.setUTCMinutes(m);
		return this;
	}

	/**
	 * Sets the UTC second
	 *
	 * @category DateTime Manipulation - Setters
	 */
	setSecond(s: number): DateTime {
		this.date.setUTCSeconds(s);
		return this;
	}

	/**
	 * Sets the UTC millisecond
	 *
	 * @category DateTime Manipulation - Setters
	 */
	setMillisecond(ms: number): DateTime {
		this.date.setUTCMilliseconds(ms);
		return this;
	}

	/**
	 * Sets the time and date to UTC start of month
	 *
	 * @category DateTime Manipulation - Special Setters
	 */
	setToStartOfMonth(): DateTime {
		this.startOf('month');
		return this;
	}

	/**
	 * Sets the time and date to UTC start of week
	 *
	 * @category DateTime Manipulation - Special Setters
	 */
	setToStartOfWeek(): DateTime {
		this.startOf('week');
		return this;
	}

	/**
	 * Sets the time and date to UTC start of month
	 *
	 * @category DateTime Manipulation - Special Setters
	 */
	setToEndOfMonth(): DateTime {
		this.endOf('month');
		return this;
	}

	/**
	 * Sets the time and date to UTC end of week
	 *
	 * @category DateTime Manipulation - Special Setters
	 */
	setToEndOfWeek(): DateTime {
		this.endOf('week');

		return this;
	}

	/**
	 * Sets the time to either 00:00:00.000 or 23:59:59.999
	 *
	 * @category DateTime Manipulation - Special Setters
	 */
	setTimeTo(zero = false) {
		return zero ? this.setTimeToZero() : this.setTimeToEndOfDay();
	}

	/**
	 * Sets the time to 00:00:00.000
	 *
	 * @category DateTime Manipulation - Special Setters
	 */
	setTimeToZero(): DateTime {
		this.setHour(0).setMinute(0).setSecond(0).setMillisecond(0);

		return this;
	}

	/**
	 * Sets the time to current time without changing date
	 *
	 * @category DateTime Manipulation - Special Setters
	 */
	setTimeToNow(): DateTime {
		const now = new DateTime();
		this.setHour(now.hour)
			.setMinute(now.minute)
			.setSecond(now.second)
			.setMillisecond(now.millisecond);
		return this;
	}

	/**
	 * Sets the time to 23:59:59.999
	 *
	 * @category DateTime Manipulation - Special Setters
	 */
	setTimeToEndOfDay(): DateTime {
		this.setHour(23)
			.setMinute(59)
			.setSecond(59)
			.setMillisecond(999);
		return this;
	}

	/**
	 * Incrementation functions:
	 */
	incSecond(): DateTime {
		return this.addSecond(1);
	}

	/**
	 * Add certain amount of years to datetime
	 *
	 * @param value number of years
	 * @category DateTime Manipulation - Add
	 */
	addYear(value: number): DateTime {
		return this.setYear(this.getYear() + value);
	}

	/**
	 * Add certain amount of months to datetime
	 *
	 * @param value number of months
	 * @category DateTime Manipulation - Add
	 */
	addMonth(value: number): DateTime {
		return this.setMonth(this.getMonth() + value);
	}

	/**
	 * Add certain amount of weeks to datetime, this is equivalent of days * 7
	 *
	 * @param value number of weeks
	 * @category DateTime Manipulation - Add
	 */
	addWeek(value: number): DateTime {
		return this.setDay(this.getDay() + Math.round(value * 7));
	}

	/**
	 * Add certain amount of days to datetime
	 *
	 * @param value number of days
	 * @category DateTime Manipulation - Add
	 */
	addDay(value: number): DateTime {
		return this.setDay(this.getDay() + value);
	}

	/**
	 * Add certain amount of hours to datetime
	 *
	 * @param value number of hours
	 * @category DateTime Manipulation - Add
	 */
	addHour(value: number): DateTime {
		return this.setPreciseTimestamp(this.getPreciseTimestamp() + Math.round(value * DateTime.MILLISECONDS_PER_HOUR));
	}

	/**
	 * Add certain amount of minutes to datetime
	 *
	 * @param value number of minutes
	 * @category DateTime Manipulation - Add
	 */
	addMinute(value: number): DateTime {
		return this.setPreciseTimestamp(this.getPreciseTimestamp() + Math.round(value * DateTime.MILLISECONDS_PER_MINUTE));
	}

	/**
	 * Add certain amount of seconds to datetime
	 *
	 * @param value number of seconds
	 * @category DateTime Manipulation - Add
	 */
	addSecond(value: number): DateTime {
		return this.setPreciseTimestamp(this.getPreciseTimestamp() + Math.round(value * DateTime.MILLISECONDS_PER_SECOND));
	}

	/**
	 * Add certain amount of milliseconds to datetime
	 *
	 * @param v number of milliseconds
	 * @category DateTime Manipulation - Add
	 */
	addMillisecond(v: number): DateTime {
		return this.setPreciseTimestamp(this.getPreciseTimestamp() + v);
	}

	/**
	 * Subtract certain amount of years from datetime
	 *
	 * @param value number of years
	 * @category DateTime Manipulation - Subtract
	 */
	subtractYear(value: number): DateTime {
		return this.addYear(-1 * value);
	}

	/**
	 * Subtract certain amount of months from datetime
	 *
	 * @param value number of months
	 * @category DateTime Manipulation - Subtract
	 */
	subtractMonth(value: number): DateTime {
		return this.addMonth(-1 * value);
	}

	/**
	 * Subtract certain amount of weeks from datetime
	 *
	 * @param value number of weeks
	 * @category DateTime Manipulation - Subtract
	 */
	subtractWeek(value: number): DateTime {
		return this.addWeek(-1 * value);
	}

	/**
	 * Subtract certain amount of days from datetime
	 *
	 * @param value number of days
	 * @category DateTime Manipulation - Subtract
	 */
	subtractDay(value: number): DateTime {
		return this.addDay(-1 * value);
	}

	/**
	 * Subtract certain amount of hours from datetime
	 *
	 * @param value number of hours
	 * @category DateTime Manipulation - Subtract
	 */
	subtractHour(value: number): DateTime {
		return this.addHour(-1 * value);
	}

	/**
	 * Subtract certain amount of minutes from datetime
	 *
	 * @param value number of minutes
	 * @category DateTime Manipulation - Subtract
	 */
	subtractMinute(value: number): DateTime {
		return this.addMinute(-1 * value);
	}

	/**
	 * Subtract certain amount of seconds from datetime
	 *
	 * @param value number of seconds
	 * @category DateTime Manipulation - Subtract
	 */
	subtractSecond(value: number): DateTime {
		return this.addSecond(-1 * value);
	}

	/**
	 * Subtract certain amount of milliseconds from datetime
	 *
	 * @param value number of milliseconds
	 * @category DateTime Manipulation - Subtract
	 */
	subtractMillisecond(value: number): DateTime {
		return this.addMillisecond(-1 * value);
	}

	/**
	 * Diff:
	 */
	diffSecond(dt: DateTime): number {
		return this.diff(dt, 'second');
	}

	/**
	 * Comparison functions:
	 */

	isBeforeDay(dt: DateTime): boolean {
		return this.isBefore(dt, 'day');
	}

	isBetween(start: DateTime, end: DateTime): boolean {
		return this.isSameOrAfter(start) && this.isSameOrBefore(end);
	}

	isSameMonth(dt: DateTime): boolean {
		return this.isSame(dt, 'month');
	}

	isSameDay(dt: DateTime): boolean {
		return this.isSame(dt, 'day');
	}
}
