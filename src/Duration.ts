import { DateTime } from './DateTime';

export class Duration {
	static FORMAT_MINUTES = 'mm:ss';
	static FORMAT_FULL_TIME = 'HH:mm:ss';

	protected constructor(
		public milliseconds = 0
	) {
	}

	static fromMilliseconds(value: number): Duration {
		return new Duration(value);
	}

	static fromSeconds(value: number): Duration {
		return new Duration(value * DateTime.MILLISECONDS_PER_SECOND);
	}

	static fromMinutes(value: number): Duration {
		return new Duration(value * DateTime.MILLISECONDS_PER_MINUTE);
	}

	static fromHours(value: number): Duration {
		return new Duration(value * DateTime.MILLISECONDS_PER_HOUR);
	}

	static fromDays(value: number): Duration {
		return new Duration(value * DateTime.MILLISECONDS_PER_DAY);
	}

	static fromWeeks(value: number): Duration {
		return new Duration(value * DateTime.MILLISECONDS_PER_WEEK);
	}

	valueOf() {
		return this.milliseconds;
	}

	getString(format = Duration.FORMAT_FULL_TIME): string {
		const dt = new DateTime(this.milliseconds);

		return dt.getString(format);
	}
}
