# Mutable vs Immutable

---

This debate is very old and every way has its own use cases. 
So DateTime Next does not impose which one you should be using, 
YOU as a developer can choose.

Mutable does not equal bad. Neither does Immutable equals good.

Key differences are (in most libraries actually, not just DateTime Next):

### Mutable:

Pros:
 * Faster
 * Does not create a clone of itself each time it's modified

Cons:
 * Can cause issues for less experienced developers
 * Requires manual cloning when you wish to preserve original object.

Use cases:
 * You do not care about original object
 * You wish to modify/parse/format a large amount of dates and times
 * You want to heavily modify date and time

### Immutable:

Pros:
 * Safer, especially for less experienced dev
 * Automatically creates clone every time it's modified
 * Technically they are also multi threaded save but JS isn't exactly a multi threaded language

Cons:

 * Slower, every operation is a clone thus takes more time
 * More memory consumption



In the end there is no winner here. **No, Immutable is not a solution.** It is just a different approach
that in some situations is better and in some - worse.

DateTime next was inspired by PHP [DateTime](https://www.php.net/manual/en/class.datetime.php) and 
[DateTimeImmutable](https://www.php.net/manual/en/class.datetimeimmutable.php) where user can actually
choose which one suits them better.