import { DateTime } from '../src/DateTime';
import { Test }     from './Test';

const formats = [
	// year
	{ input: '2021-06-09T12:42:40.793Z', format: 'YYYY', out: '2021' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'YY', out: '21' },
	// month
	{ input: '2021-06-09T12:42:40.793Z', format: 'M', out: '6' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'MM', out: '06' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'MMM', out: 'Jun' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'MMMM', out: 'June' },
	// weekday
	{ input: '2021-06-09T12:42:40.793Z', format: 'd', out: '3' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'dd', out: 'W' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'ddd', out: 'Wed' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'dddd', out: 'Wednesday' },
	// day
	{ input: '2021-06-09T12:42:40.793Z', format: 'D', out: '9' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'DD', out: '09' },
	// hour
	{ input: '2021-06-09T02:42:40.793Z', format: 'H', out: '2' },
	{ input: '2021-06-09T02:42:40.793Z', format: 'HH', out: '02' },
	// hour 12
	{ input: '2021-06-09T14:42:40.793Z', format: 'h', out: '2' },
	{ input: '2021-06-09T14:42:40.793Z', format: 'hh', out: '02' },
	// meridian
	{ input: '2021-06-09T14:42:40.793Z', format: 'a', out: 'pm' },
	{ input: '2021-06-09T14:42:40.793Z', format: 'A', out: 'PM' },
	// minute
	{ input: '2021-06-09T14:07:07.793Z', format: 'm', out: '7' },
	{ input: '2021-06-09T14:07:07.793Z', format: 'mm', out: '07' },
	// second
	{ input: '2021-06-09T14:42:09.793Z', format: 's', out: '9' },
	{ input: '2021-06-09T14:42:09.793Z', format: 'ss', out: '09' },
	// millisecond
	{ input: '2021-06-09T14:42:09.793Z', format: 'SSS', out: '793' },

	{ input: '2021-06-09T12:42:40.793Z', format: 'YYYY-MM-DD HH:mm:ss.SSS', out: '2021-06-09 12:42:40.793' },
	{ input: '2021-06-09T12:42:40.793Z', format: 'YYYY-MM-DD HH:mm:ss', out: '2021-06-09 12:42:40' }
];
console.log('\nFormatting iso');
for (const t of formats) {
	Test.run(`'${ t.input }' as input formatted with '${ t.format }' should be equal '${ t.out }'`, () => {
		DateTime.setDefaultLocale('en-US');
		const dt = new DateTime(t.input);
		const result = dt.getString(t.format);
		if (result !== t.out) {
			return result;
		}
		return true;
	});
}
const localFormats = [// zone
	{ input: '2021-06-09T14:42:09.793Z', format: 'Z', out: '793' },
	{ input: '2021-06-09T14:42:09.793Z', format: 'ZZ', out: '793' }
];

console.log('\nLocal format');
for (const t of localFormats) {
	Test.run(`'${ t.input }' as input formatted with '${ t.format }' should be equal '${ t.out }'`, () => {
		DateTime.setDefaultLocale('en-US');
		DateTime.setDefaultTimezone('America/New_York');
		const dt = new DateTime(t.input);
		const result = dt.getString(t.format);
		if (result !== t.out) {
			return result;
		}
		return true;
	});
}

