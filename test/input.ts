import { DateTime } from '../src/DateTime';
import { Test } from './Test';


const stringInputs = [
	{ input: '2021-06-09 12:42:19.793Z', iso: '2021-06-09T12:42:19.793Z' },
	{ input: '2021-06-09T12:42:19.793Z', iso: '2021-06-09T12:42:19.793Z' },
	{ input: '2021-06-09T12:42:19', iso: '2021-06-09T12:42:19.000Z' },
	{ input: '2021-06-09T12:42', iso: '2021-06-09T12:42:00.000Z' },
	{ input: '2021-06-09T12', iso: '2021-06-09T12:00:00.000Z' },
	{ input: '2021-06-09', iso: '2021-06-09T00:00:00.000Z' },
	{ input: '2021-06', iso: '2021-06-01T00:00:00.000Z' },
	{ input: '2021', iso: '2021-01-01T00:00:00.000Z' },
	{ input: 1234123512343, iso: '2009-02-08T20:05:12.343Z' }
];

console.log('String inputs (T is optional so results would be the same with space in there)');
for (const t of stringInputs) {
	Test.run(`'${ t.input }' Should be equal to '${ t.iso }'`, () => {
		const r1 = (new DateTime(t.input)).toISOString();
		if (r1 !== t.iso) {
			return r1;
		}
		return true;
	});
}

const staticInputs = [
	{ input: 1623279703886, method: 'fromPreciseTimestamp', iso: new Date(1623279703886).toISOString() },
	{ input: 1623279703, method: 'fromTimestamp', iso: new Date(1623279703 * 1000).toISOString() },
	{ input: 1623279703886, method: 'fromUtcString', iso: new Date(1623279703886).toISOString() },
	{ input: 1623279703886, method: 'fromUtcPreciseString', iso: new Date(1623279703886).toISOString() },
	{ input: 1623279703886, method: 'fromLocalPreciseString', iso: new Date(1623279703886).toISOString() },

];
console.log('\nStatic inputs');
for (const t of staticInputs) {
	Test.run(`'${ t.input }' using method ${ t.method } Should be equal '${ t.iso }'`, () => {
		const dt = DateTime[t.method](t.input);
		if (dt.toISOString() !== t.iso) {
			return dt.toISOString();
		}
		return true;
	});
}

const invalidInputs = [
	{ input: '2021-06-09T12:42:60.793Z' },
	{ input: '2021-06-09T12:67' },
	{ input: '2021-06-09T25' },
	{ input: '2021-06-32' },
	{ input: '2021-13' }
];

console.log('\nInvalid inputs');
for (const t of invalidInputs) {
	Test.run(`'${ t.input }' Should be invalid`, () => {
		return !(new DateTime(t.input)).isValid();
	});
}
