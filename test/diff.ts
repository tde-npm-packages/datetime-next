import { DateTime } from '../src/DateTime';
import { Test } from './Test';

const diff = [
	{ a: '2021-06-09T12:42:40.793Z', b: '2021-06-09T12:42:40.893Z', out: -100 },
	{ a: '2021-06-09T12:42:40.793Z', b: '2021-06-09T12:42:42.893Z', out: -(100 + 2 * 1000) },
	{ a: '2021-06-09T12:42:40.793Z', b: '2021-06-09T12:46:42.893Z', out: -(100 + 2 * 1000 + 4 * 60 * 1000) },
	{ a: '2021-06-09T12:42:40.793Z', b: '2021-06-09T17:46:42.893Z', out: -(100 + 2 * 1000 + 4 * 60 * 1000 + 5 * 60 * 60 * 1000) },
];

console.log('\nDate diff - value');
for (const t of diff) {
	Test.run(`'${ t.a }' as input diff with '${ t.b }', should be equal ${ t. out }ms`, () => {
		const a = new DateTime(t.a);
		const b = new DateTime(t.b);
		const d = a.diff(b);

		if (d !== t.out) {
			return d;
		}
		return true;
	});
}
