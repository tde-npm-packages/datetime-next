import { DateTime } from '../src/DateTime';
import { Test } from './Test';

const addInputs = [
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:42:40.794Z', unit: 'millisecond', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:42:41.793Z', unit: 'second', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:43:40.793Z', unit: 'minute', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T13:42:40.793Z', unit: 'hour', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-10T12:42:40.793Z', unit: 'day', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-07-09T12:42:40.793Z', unit: 'month', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2022-06-09T12:42:40.793Z', unit: 'year', v: 1 }
];

console.log('\nDate manipulation - add');
for (const t of addInputs) {
	Test.run(`'${ t.input }' plus ${ t.v } ${ t.unit } Should be equal ${ t.iso }`, () => {
		const dt = new DateTime(t.input);
		dt.add(t.v, t.unit as any);
		if (dt.toISOString() !== t.iso) {
			return dt.toISOString();
		}
		return true;
	});
}

const edgeManipulation = [
	// edge cases
	{ input: '2021-06-09T12:42:40.793Z', iso: '2022-08-09T12:42:40.793Z', unit: 'month', v: 14 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-07-14T12:42:40.793Z', unit: 'week', v: 5 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-07-19T12:42:40.793Z', unit: 'day', v: 40 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-10T18:42:40.793Z', unit: 'hour', v: 30 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T14:57:40.793Z', unit: 'minute', v: 135 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:45:07.793Z', unit: 'second', v: 147 },
	{ input: '2021-12-31T23:59:59.999Z', iso: '2022-01-01T00:00:00.000Z', unit: 'millisecond', v: 1 }
];
console.log('\nDate manipulation - add edge cases');
for (const t of edgeManipulation) {
	Test.run(`'${ t.input }' plus ${ t.v } ${ t.unit } Should be equal ${ t.iso }`, () => {
		const dt = new DateTime(t.input);
		dt.add(t.v, t.unit as any);
		if (dt.toISOString() !== t.iso) {
			return dt.toISOString();
		}
		return true;
	});
}