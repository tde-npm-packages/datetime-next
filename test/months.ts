import { DateTime } from '../src/DateTime';
import { Test }     from './Test';

console.log('\nMonth names');
const monthNames = [
	{ input: 1, out: 'January' },
	{ input: 2, out: 'February' },
	{ input: 3, out: 'March' },
	{ input: 4, out: 'April' },
	{ input: 5, out: 'May' },
	{ input: 6, out: 'June' },
	{ input: 7, out: 'July' },
	{ input: 8, out: 'August' },
	{ input: 9, out: 'September' },
	{ input: 10, out: 'October' },
	{ input: 11, out: 'November' },
	{ input: 12, out: 'December' }
];

DateTime.setDefaultLocale('en-Us');
for (const t of monthNames) {
	Test.run(`month ${ t.input } Should be equal '${ t.out }'`, () => {
		// otherwise your default locale gets used
		const dt = DateTime.getMonthName(t.input, 'long');
		if (dt !== t.out) {
			return dt;
		}
		return dt === t.out;
	});
}