import { DateTime } from '../src/DateTime';
import { Test } from './Test';

const startOf = [
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:42:40.000Z', unit: 'second', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:42:00.000Z', unit: 'minute', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:00:00.000Z', unit: 'hour', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T00:00:00.000Z', unit: 'day', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-07T00:00:00.000Z', unit: 'week', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-01T00:00:00.000Z', unit: 'month', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-01-01T00:00:00.000Z', unit: 'year', v: 1 }
];

console.log('\nDate manipulation - startOf');
for (const t of startOf) {
	Test.run(`'${ t.input }' as input, modified by start of ${ t.unit } should be equal ${ t.iso }`, () => {
		const dt = new DateTime(t.input);
		dt.startOf(t.unit as any)
		if (dt.toISOString() !== t.iso) {
			return dt.toISOString();
		}
		return true;
	});
}

const endOfInputs = [
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:42:40.999Z', unit: 'second', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:42:59.999Z', unit: 'minute', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T12:59:59.999Z', unit: 'hour', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-09T23:59:59.999Z', unit: 'day', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-13T23:59:59.999Z', unit: 'week', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-06-30T23:59:59.999Z', unit: 'month', v: 1 },
	{ input: '2021-06-09T12:42:40.793Z', iso: '2021-12-31T23:59:59.999Z', unit: 'year', v: 1 }
];

console.log('\nDate manipulation - endOf');
for (const t of endOfInputs) {
	Test.run(`'${ t.input }' as input, modified by end of ${ t.unit } should be equal ${ t.iso }`, () => {
		const dt = new DateTime(t.input);
		dt.endOf(t.unit as any)
		if (dt.toISOString() !== t.iso) {
			return dt.toISOString();
		}
		return true;
	});
}