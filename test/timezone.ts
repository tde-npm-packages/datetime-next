import { TimeZone } from '../src/TimeZone';
import { DateTime } from '../src/DateTime';
import { Test }     from './Test';

const startOf = [
	{ input: '2021-06-09T12:42:40.793Z', timeZone: 'Europe/Warsaw', out: -120 },
	{ input: '2021-06-09T12:42:40.793Z', timeZone: 'Asia/Tehran', out: -270 },
	{ input: '2021-06-09T12:42:40.793Z', timeZone: 'America/Toronto', out: 240 },
	{ input: '2021-06-09T12:42:40.793Z', timeZone: 'Africa/Kigali', out: -120 },
	{ input: '2021-06-09T12:42:40.793Z', timeZone: 'Africa/Algiers', out: -60 },
	{ input: '2021-06-09T12:42:40.793Z', timeZone: 'Africa/Abidjan', out: 0 },
	{ input: '2021-06-09T12:42:40.793Z', timeZone: 'Pacific/Chatham', out: -765 },
	{ input: '2021-02-09T12:42:40.793Z', timeZone: 'Pacific/Chatham', out: -825 }, // dst
];

console.log('\nTimezone offset');
for (const t of startOf) {
	Test.run(`'${ t.input }' as input, with timezone '${t.timeZone}' should be equal ${ t.out }`, () => {
		const dt = new DateTime(t.input);
		const offset = TimeZone.offset(t.timeZone, dt)
		if (offset !== t.out) {
			return offset;
		}
		return true;
	});
}