import { DateTime } from '../src/DateTime';
import { Test }     from './Test';

console.log('\nweek names');
const weekNames = [
	{ input: 1, out: 'Monday' },
	{ input: 2, out: 'Tuesday' },
	{ input: 3, out: 'Wednesday' },
	{ input: 4, out: 'Thursday' },
	{ input: 5, out: 'Friday' },
	{ input: 6, out: 'Saturday' },
	{ input: 7, out: 'Sunday' },
];

DateTime.setDefaultLocale('en-Us');
for (const t of weekNames) {
	Test.run(`week ${ t.input } Should be equal '${ t.out }'`, () => {
		// otherwise your default locale gets used
		const dt = DateTime.getWeekName(t.input, 'long');
		if (dt !== t.out) {
			return dt;
		}
		return dt === t.out;
	});
}