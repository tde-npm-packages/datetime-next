import { DateTime } from '../src/DateTime';
import { Test }     from './Test';
// end 2021-12-31T23:59:59.999Z
const formats = [
	// year
	{ input: '2022-01-01T00:00:00.000Z', format: 'YYYY', out: '2021' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'YY', out: '21' },
	// month
	{ input: '2022-01-01T00:00:00.000Z', format: 'M', out: '12' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'MM', out: '12' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'MMM', out: 'Dec' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'MMMM', out: 'December' },
	// weekday
	{ input: '2022-01-01T00:00:00.000Z', format: 'd', out: '5' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'dd', out: 'F' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'ddd', out: 'Fri' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'dddd', out: 'Friday' },
	// day
	{ input: '2022-01-01T00:00:00.000Z', format: 'D', out: '31' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'DD', out: '31' },
	// hour
	{ input: '2022-01-01T00:00:00.000Z', format: 'H', out: '19' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'HH', out: '19' },
	// hour 12
	{ input: '2022-01-01T00:00:00.000Z', format: 'h', out: '7' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'hh', out: '07' },
	// meridian
	{ input: '2022-01-01T00:00:00.000Z', format: 'a', out: 'pm' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'A', out: 'PM' },
	// minute
	{ input: '2022-01-01T00:00:00.000Z', format: 'm', out: '0' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'mm', out: '00' },
	// second
	{ input: '2022-01-01T00:00:00.000Z', format: 's', out: '0' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'ss', out: '00' },
	// millisecond
	{ input: '2022-01-01T00:00:00.000Z', format: 'SSS', out: '000' },

	{ input: '2022-01-01T00:00:00.000Z', format: 'YYYY-MM-DD HH:mm:ss.SSS', out: '2021-12-31 19:00:00.000' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'YYYY-MM-DD HH:mm:ss', out: '2021-12-31 19:00:00' }
];

DateTime.setDefaultLocale('en-US');
DateTime.setDefaultTimezone('America/New_York');

console.log('\nFormatting local time');
for (const t of formats) {
	Test.run(`'${ t.input }' as input formatted with '${ t.format }' with NY locale should be equal '${ t.out }'`, () => {
		const dt = new DateTime(t.input);
		const result = dt.getString(t.format, true);
		if (result !== t.out) {
			return result;
		}
		return true;
	});
}
const localFormats = [// zone
	{ input: '2022-01-01T00:00:00.000Z', format: 'Z', out: '+05:00' },
	{ input: '2022-01-01T00:00:00.000Z', format: 'ZZ', out: '+0500' }
];

console.log('\nLocal format');
for (const t of localFormats) {
	Test.run(`'${ t.input }' as input formatted with '${ t.format }' should be equal '${ t.out }'`, () => {
		const dt = new DateTime(t.input);
		const result = dt.getString(t.format, true);
		if (result !== t.out) {
			return result;
		}
		return true;
	});
}

