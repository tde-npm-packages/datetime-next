[datetime-next](../README.md) / [Exports](../modules.md) / Duration

# Class: Duration

## Table of contents

### Constructors

- [constructor](duration.md#constructor)

### Properties

- [milliseconds](duration.md#milliseconds)
- [FORMAT\_FULL\_TIME](duration.md#format_full_time)
- [FORMAT\_MINUTES](duration.md#format_minutes)

### Methods

- [getString](duration.md#getstring)
- [valueOf](duration.md#valueof)
- [fromDays](duration.md#fromdays)
- [fromHours](duration.md#fromhours)
- [fromMilliseconds](duration.md#frommilliseconds)
- [fromMinutes](duration.md#fromminutes)
- [fromSeconds](duration.md#fromseconds)
- [fromWeeks](duration.md#fromweeks)

## Constructors

### constructor

• `Protected` **new Duration**(`milliseconds?`)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `milliseconds` | `number` | 0 |

## Properties

### milliseconds

• **milliseconds**: `number` = 0

___

### FORMAT\_FULL\_TIME

▪ `Static` **FORMAT\_FULL\_TIME**: `string` = 'HH:mm:ss'

___

### FORMAT\_MINUTES

▪ `Static` **FORMAT\_MINUTES**: `string` = 'mm:ss'

## Methods

### getString

▸ **getString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

___

### valueOf

▸ **valueOf**(): `number`

#### Returns

`number`

___

### fromDays

▸ `Static` **fromDays**(`value`): [Duration](duration.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `number` |

#### Returns

[Duration](duration.md)

___

### fromHours

▸ `Static` **fromHours**(`value`): [Duration](duration.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `number` |

#### Returns

[Duration](duration.md)

___

### fromMilliseconds

▸ `Static` **fromMilliseconds**(`value`): [Duration](duration.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `number` |

#### Returns

[Duration](duration.md)

___

### fromMinutes

▸ `Static` **fromMinutes**(`value`): [Duration](duration.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `number` |

#### Returns

[Duration](duration.md)

___

### fromSeconds

▸ `Static` **fromSeconds**(`value`): [Duration](duration.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `number` |

#### Returns

[Duration](duration.md)

___

### fromWeeks

▸ `Static` **fromWeeks**(`value`): [Duration](duration.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `number` |

#### Returns

[Duration](duration.md)
