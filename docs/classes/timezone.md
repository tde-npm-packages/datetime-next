[datetime-next](../README.md) / [Exports](../modules.md) / TimeZone

# Class: TimeZone

## Table of contents

### Constructors

- [constructor](timezone.md#constructor)

### Properties

- [dtfCache](timezone.md#dtfcache)
- [formatOptions](timezone.md#formatoptions)
- [locale](timezone.md#locale)
- [usRegex](timezone.md#usregex)

### Methods

- [isZoneValid](timezone.md#iszonevalid)
- [offset](timezone.md#offset)
- [parseDate](timezone.md#parsedate)
- [parseGMTOffset](timezone.md#parsegmtoffset)

## Constructors

### constructor

• **new TimeZone**()

## Properties

### dtfCache

▪ `Static` **dtfCache**: `Object` = {}

___

### formatOptions

▪ `Static` **formatOptions**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `day` | `string` |
| `hour` | `string` |
| `hourCycle` | `string` |
| `minute` | `string` |
| `month` | `string` |
| `timeZone` | `string` |
| `year` | `string` |

___

### locale

▪ `Static` **locale**: `string` = 'en-US'

___

### usRegex

▪ `Static` **usRegex**: `RegExp`

## Methods

### isZoneValid

▸ `Static` **isZoneValid**(`zone`): `boolean`

Checks if IANA timezone is valid

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `zone` | `string` | zone name ex 'America/Toronto' |

#### Returns

`boolean`

___

### offset

▸ `Static` **offset**(`timeZone`, `date?`): `number`

Calculate timezone offset for IANA timezone

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `timeZone` | `string` | zone name ex 'America/Toronto' |
| `date?` | [DateTime](datetime.md) \| `Date` | optional date to calculate offset (for DST) otherwise current date is taken |

#### Returns

`number`

___

### parseDate

▸ `Static` **parseDate**(`dateString`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `dateString` | `string` |

#### Returns

`any`

___

### parseGMTOffset

▸ `Static` **parseGMTOffset**(`specifier`): `number`

Parse GTM offset such as 'GMT+4:30' to minute offset from UTC

#### Parameters

| Name | Type |
| :------ | :------ |
| `specifier` | `string` |

#### Returns

`number`
