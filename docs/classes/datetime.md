[datetime-next](../README.md) / [Exports](../modules.md) / DateTime

# Class: DateTime

Next generation super slim Date wrapper

basic usage:
```ts
const dt = new DateTime('2021-06-12');
```

## Hierarchy

- **DateTime**

  ↳ [DateTimeImmutable](datetimeimmutable.md)

## Implements

- `DateInterface`
- `TimeInterface`

## Table of contents

### Constructors

- [constructor](datetime.md#constructor)

### Properties

- [date](datetime.md#date)
- [localOffset](datetime.md#localoffset)
- [locale](datetime.md#locale)
- [timeZone](datetime.md#timezone)
- [DATE\_FORMAT](datetime.md#date_format)
- [DATE\_TIME\_FORMAT](datetime.md#date_time_format)
- [DATE\_TIME\_FORMAT\_SHORT](datetime.md#date_time_format_short)
- [PRECISE\_DATE\_TIME\_FORMAT](datetime.md#precise_date_time_format)
- [PRECISE\_TIME\_FORMAT](datetime.md#precise_time_format)
- [TIME\_FORMAT](datetime.md#time_format)
- [TIME\_FORMAT\_SHORT](datetime.md#time_format_short)

### Accessors

- [dateString](datetime.md#datestring)
- [day](datetime.md#day)
- [hour](datetime.md#hour)
- [isoString](datetime.md#isostring)
- [isoWeekday](datetime.md#isoweekday)
- [localString](datetime.md#localstring)
- [localStringShort](datetime.md#localstringshort)
- [millisecond](datetime.md#millisecond)
- [minute](datetime.md#minute)
- [month](datetime.md#month)
- [monthName](datetime.md#monthname)
- [preciseTimestamp](datetime.md#precisetimestamp)
- [second](datetime.md#second)
- [timeString](datetime.md#timestring)
- [unixTimestamp](datetime.md#unixtimestamp)
- [weekday](datetime.md#weekday)
- [year](datetime.md#year)

### DateTime Manipulation - Add Methods

- [addDay](datetime.md#addday)
- [addHour](datetime.md#addhour)
- [addMillisecond](datetime.md#addmillisecond)
- [addMinute](datetime.md#addminute)
- [addMonth](datetime.md#addmonth)
- [addSecond](datetime.md#addsecond)
- [addWeek](datetime.md#addweek)
- [addYear](datetime.md#addyear)

### DateTime Manipulation - Setters Methods

- [setDay](datetime.md#setday)
- [setHour](datetime.md#sethour)
- [setMillisecond](datetime.md#setmillisecond)
- [setMinute](datetime.md#setminute)
- [setMonth](datetime.md#setmonth)
- [setSecond](datetime.md#setsecond)
- [setWeekday](datetime.md#setweekday)
- [setYear](datetime.md#setyear)

### DateTime Manipulation - Special Setters Methods

- [setTimeTo](datetime.md#settimeto)
- [setTimeToEndOfDay](datetime.md#settimetoendofday)
- [setTimeToNow](datetime.md#settimetonow)
- [setTimeToZero](datetime.md#settimetozero)
- [setToEndOfMonth](datetime.md#settoendofmonth)
- [setToEndOfWeek](datetime.md#settoendofweek)
- [setToStartOfMonth](datetime.md#settostartofmonth)
- [setToStartOfWeek](datetime.md#settostartofweek)

### DateTime Manipulation - Subtract Methods

- [subtractDay](datetime.md#subtractday)
- [subtractHour](datetime.md#subtracthour)
- [subtractMillisecond](datetime.md#subtractmillisecond)
- [subtractMinute](datetime.md#subtractminute)
- [subtractMonth](datetime.md#subtractmonth)
- [subtractSecond](datetime.md#subtractsecond)
- [subtractWeek](datetime.md#subtractweek)
- [subtractYear](datetime.md#subtractyear)

### Other Methods

- [add](datetime.md#add)
- [clone](datetime.md#clone)
- [daysInMonth](datetime.md#daysinmonth)
- [diff](datetime.md#diff)
- [diffSecond](datetime.md#diffsecond)
- [endOf](datetime.md#endof)
- [get](datetime.md#get)
- [getDateObject](datetime.md#getdateobject)
- [getDateString](datetime.md#getdatestring)
- [getDay](datetime.md#getday)
- [getHour](datetime.md#gethour)
- [getIsoString](datetime.md#getisostring)
- [getIsoWeekday](datetime.md#getisoweekday)
- [getLocalPreciseString](datetime.md#getlocalprecisestring)
- [getLocalString](datetime.md#getlocalstring)
- [getLocalStringShort](datetime.md#getlocalstringshort)
- [getMeridian](datetime.md#getmeridian)
- [getMillisecond](datetime.md#getmillisecond)
- [getMinute](datetime.md#getminute)
- [getMonth](datetime.md#getmonth)
- [getMonthName](datetime.md#getmonthname)
- [getPreciseString](datetime.md#getprecisestring)
- [getPreciseTimestamp](datetime.md#getprecisetimestamp)
- [getSecond](datetime.md#getsecond)
- [getString](datetime.md#getstring)
- [getTimeString](datetime.md#gettimestring)
- [getTimezoneOffset](datetime.md#gettimezoneoffset)
- [getTimezoneOffsetString](datetime.md#gettimezoneoffsetstring)
- [getUnixTimestamp](datetime.md#getunixtimestamp)
- [getWeekday](datetime.md#getweekday)
- [getWeekdayName](datetime.md#getweekdayname)
- [getYear](datetime.md#getyear)
- [incSecond](datetime.md#incsecond)
- [isAfter](datetime.md#isafter)
- [isBefore](datetime.md#isbefore)
- [isBeforeDay](datetime.md#isbeforeday)
- [isBetween](datetime.md#isbetween)
- [isSame](datetime.md#issame)
- [isSameDay](datetime.md#issameday)
- [isSameMonth](datetime.md#issamemonth)
- [isSameOrAfter](datetime.md#issameorafter)
- [isSameOrBefore](datetime.md#issameorbefore)
- [isValid](datetime.md#isvalid)
- [set](datetime.md#set)
- [setPreciseTimestamp](datetime.md#setprecisetimestamp)
- [setUnixTimestamp](datetime.md#setunixtimestamp)
- [startOf](datetime.md#startof)
- [stripDate](datetime.md#stripdate)
- [stripTime](datetime.md#striptime)
- [subtract](datetime.md#subtract)
- [toDate](datetime.md#todate)
- [toISOString](datetime.md#toisostring)
- [toJSON](datetime.md#tojson)
- [toString](datetime.md#tostring)
- [valueOf](datetime.md#valueof)
- [clearMonthNamesOverride](datetime.md#clearmonthnamesoverride)
- [clearWeekNamesOverride](datetime.md#clearweeknamesoverride)
- [fromLocalPreciseString](datetime.md#fromlocalprecisestring)
- [fromPreciseTimestamp](datetime.md#fromprecisetimestamp)
- [fromUnixTimestamp](datetime.md#fromunixtimestamp)
- [fromUtcPreciseString](datetime.md#fromutcprecisestring)
- [fromUtcString](datetime.md#fromutcstring)
- [getMonthName](datetime.md#getmonthname)
- [getMonthNames](datetime.md#getmonthnames)
- [getNowString](datetime.md#getnowstring)
- [getPreciseNowString](datetime.md#getprecisenowstring)
- [getPreciseTimestamp](datetime.md#getprecisetimestamp)
- [getUnixTimestamp](datetime.md#getunixtimestamp)
- [getWeekName](datetime.md#getweekname)
- [getWeekNames](datetime.md#getweeknames)
- [isValid](datetime.md#isvalid)
- [monthDiff](datetime.md#monthdiff)
- [parseDate](datetime.md#parsedate)
- [setDefaultLocale](datetime.md#setdefaultlocale)
- [setDefaultTimezone](datetime.md#setdefaulttimezone)
- [setMonthNamesOverride](datetime.md#setmonthnamesoverride)
- [setWeekNamesOverride](datetime.md#setweeknamesoverride)

## Constructors

### constructor

• **new DateTime**(`input?`)

Constructs a `DateTime` object based on provided data, input options:

* `number` - milliseconds from unix epoch
* `string` - ISO datetime format: `2020-09-23T17:23:11+04:30` also accepts partial data (only year is required)
* `Date` - Javascript `Date` object
* `DateTime` - another instance of `DateTime` (clone will be performed)

#### Parameters

| Name | Type |
| :------ | :------ |
| `input?` | `string` \| `number` \| [DateTime](datetime.md) \| `Date` |

## Properties

### date

• **date**: `Date`

___

### localOffset

• **localOffset**: `number` = 0

___

### locale

• **locale**: `string`

___

### timeZone

• **timeZone**: `string`

___

### DATE\_FORMAT

▪ `Static` **DATE\_FORMAT**: `string` = 'YYYY-MM-DD'

___

### DATE\_TIME\_FORMAT

▪ `Static` **DATE\_TIME\_FORMAT**: `string`

___

### DATE\_TIME\_FORMAT\_SHORT

▪ `Static` **DATE\_TIME\_FORMAT\_SHORT**: `string`

___

### PRECISE\_DATE\_TIME\_FORMAT

▪ `Static` **PRECISE\_DATE\_TIME\_FORMAT**: `string`

___

### PRECISE\_TIME\_FORMAT

▪ `Static` **PRECISE\_TIME\_FORMAT**: `string` = 'HH:mm:ss.SSS'

___

### TIME\_FORMAT

▪ `Static` **TIME\_FORMAT**: `string` = 'HH:mm:ss'

___

### TIME\_FORMAT\_SHORT

▪ `Static` **TIME\_FORMAT\_SHORT**: `string` = 'HH:mm'

## Accessors

### dateString

• `get` **dateString**(): `string`

#### Returns

`string`

___

### day

• `get` **day**(): `number`

#### Returns

`number`

___

### hour

• `get` **hour**(): `number`

#### Returns

`number`

___

### isoString

• `get` **isoString**(): `string`

#### Returns

`string`

___

### isoWeekday

• `get` **isoWeekday**(): `number`

#### Returns

`number`

___

### localString

• `get` **localString**(): `string`

#### Returns

`string`

___

### localStringShort

• `get` **localStringShort**(): `string`

#### Returns

`string`

___

### millisecond

• `get` **millisecond**(): `number`

#### Returns

`number`

___

### minute

• `get` **minute**(): `number`

#### Returns

`number`

___

### month

• `get` **month**(): `number`

#### Returns

`number`

___

### monthName

• `get` **monthName**(): `string`

#### Returns

`string`

___

### preciseTimestamp

• `get` **preciseTimestamp**(): `number`

#### Returns

`number`

___

### second

• `get` **second**(): `number`

#### Returns

`number`

___

### timeString

• `get` **timeString**(): `string`

#### Returns

`string`

___

### unixTimestamp

• `get` **unixTimestamp**(): `number`

#### Returns

`number`

___

### weekday

• `get` **weekday**(): `number`

#### Returns

`number`

___

### year

• `get` **year**(): `number`

#### Returns

`number`

## DateTime Manipulation - Add Methods

### addDay

▸ **addDay**(`value`): [DateTime](datetime.md)

Add certain amount of days to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of days |

#### Returns

[DateTime](datetime.md)

___

### addHour

▸ **addHour**(`value`): [DateTime](datetime.md)

Add certain amount of hours to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of hours |

#### Returns

[DateTime](datetime.md)

___

### addMillisecond

▸ **addMillisecond**(`v`): [DateTime](datetime.md)

Add certain amount of milliseconds to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `v` | `number` | number of milliseconds |

#### Returns

[DateTime](datetime.md)

___

### addMinute

▸ **addMinute**(`value`): [DateTime](datetime.md)

Add certain amount of minutes to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of minutes |

#### Returns

[DateTime](datetime.md)

___

### addMonth

▸ **addMonth**(`value`): [DateTime](datetime.md)

Add certain amount of months to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of months |

#### Returns

[DateTime](datetime.md)

___

### addSecond

▸ **addSecond**(`value`): [DateTime](datetime.md)

Add certain amount of seconds to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of seconds |

#### Returns

[DateTime](datetime.md)

___

### addWeek

▸ **addWeek**(`value`): [DateTime](datetime.md)

Add certain amount of weeks to datetime, this is equivalent of days * 7

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of weeks |

#### Returns

[DateTime](datetime.md)

___

### addYear

▸ **addYear**(`value`): [DateTime](datetime.md)

Add certain amount of years to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of years |

#### Returns

[DateTime](datetime.md)

___

## DateTime Manipulation - Setters Methods

### setDay

▸ **setDay**(`d`): [DateTime](datetime.md)

Sets the UTC day of month (not weekday)

#### Parameters

| Name | Type |
| :------ | :------ |
| `d` | `number` |

#### Returns

[DateTime](datetime.md)

___

### setHour

▸ **setHour**(`h`): [DateTime](datetime.md)

Sets the UTC hour

#### Parameters

| Name | Type |
| :------ | :------ |
| `h` | `number` |

#### Returns

[DateTime](datetime.md)

___

### setMillisecond

▸ **setMillisecond**(`ms`): [DateTime](datetime.md)

Sets the UTC millisecond

#### Parameters

| Name | Type |
| :------ | :------ |
| `ms` | `number` |

#### Returns

[DateTime](datetime.md)

___

### setMinute

▸ **setMinute**(`m`): [DateTime](datetime.md)

Sets the UTC minute

#### Parameters

| Name | Type |
| :------ | :------ |
| `m` | `number` |

#### Returns

[DateTime](datetime.md)

___

### setMonth

▸ **setMonth**(`m`): [DateTime](datetime.md)

Sets the UTC Month

#### Parameters

| Name | Type |
| :------ | :------ |
| `m` | `number` |

#### Returns

[DateTime](datetime.md)

___

### setSecond

▸ **setSecond**(`s`): [DateTime](datetime.md)

Sets the UTC second

#### Parameters

| Name | Type |
| :------ | :------ |
| `s` | `number` |

#### Returns

[DateTime](datetime.md)

___

### setWeekday

▸ **setWeekday**(`week`): [DateTime](datetime.md)

Sets the UTC weekday

#### Parameters

| Name | Type |
| :------ | :------ |
| `week` | `number` |

#### Returns

[DateTime](datetime.md)

___

### setYear

▸ **setYear**(`y`): [DateTime](datetime.md)

Sets the UTC Year

#### Parameters

| Name | Type |
| :------ | :------ |
| `y` | `number` |

#### Returns

[DateTime](datetime.md)

___

## DateTime Manipulation - Special Setters Methods

### setTimeTo

▸ **setTimeTo**(`zero?`): [DateTime](datetime.md)

Sets the time to either 00:00:00.000 or 23:59:59.999

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `zero` | `boolean` | false |

#### Returns

[DateTime](datetime.md)

___

### setTimeToEndOfDay

▸ **setTimeToEndOfDay**(): [DateTime](datetime.md)

Sets the time to 23:59:59.999

#### Returns

[DateTime](datetime.md)

___

### setTimeToNow

▸ **setTimeToNow**(): [DateTime](datetime.md)

Sets the time to current time without changing date

#### Returns

[DateTime](datetime.md)

___

### setTimeToZero

▸ **setTimeToZero**(): [DateTime](datetime.md)

Sets the time to 00:00:00.000

#### Returns

[DateTime](datetime.md)

___

### setToEndOfMonth

▸ **setToEndOfMonth**(): [DateTime](datetime.md)

Sets the time and date to UTC start of month

#### Returns

[DateTime](datetime.md)

___

### setToEndOfWeek

▸ **setToEndOfWeek**(): [DateTime](datetime.md)

Sets the time and date to UTC end of week

#### Returns

[DateTime](datetime.md)

___

### setToStartOfMonth

▸ **setToStartOfMonth**(): [DateTime](datetime.md)

Sets the time and date to UTC start of month

#### Returns

[DateTime](datetime.md)

___

### setToStartOfWeek

▸ **setToStartOfWeek**(): [DateTime](datetime.md)

Sets the time and date to UTC start of week

#### Returns

[DateTime](datetime.md)

___

## DateTime Manipulation - Subtract Methods

### subtractDay

▸ **subtractDay**(`value`): [DateTime](datetime.md)

Subtract certain amount of days from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of days |

#### Returns

[DateTime](datetime.md)

___

### subtractHour

▸ **subtractHour**(`value`): [DateTime](datetime.md)

Subtract certain amount of hours from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of hours |

#### Returns

[DateTime](datetime.md)

___

### subtractMillisecond

▸ **subtractMillisecond**(`value`): [DateTime](datetime.md)

Subtract certain amount of milliseconds from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of milliseconds |

#### Returns

[DateTime](datetime.md)

___

### subtractMinute

▸ **subtractMinute**(`value`): [DateTime](datetime.md)

Subtract certain amount of minutes from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of minutes |

#### Returns

[DateTime](datetime.md)

___

### subtractMonth

▸ **subtractMonth**(`value`): [DateTime](datetime.md)

Subtract certain amount of months from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of months |

#### Returns

[DateTime](datetime.md)

___

### subtractSecond

▸ **subtractSecond**(`value`): [DateTime](datetime.md)

Subtract certain amount of seconds from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of seconds |

#### Returns

[DateTime](datetime.md)

___

### subtractWeek

▸ **subtractWeek**(`value`): [DateTime](datetime.md)

Subtract certain amount of weeks from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of weeks |

#### Returns

[DateTime](datetime.md)

___

### subtractYear

▸ **subtractYear**(`value`): [DateTime](datetime.md)

Subtract certain amount of years from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of years |

#### Returns

[DateTime](datetime.md)

___

## Other Methods

### add

▸ **add**(`value`, `unit`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `number` |
| `unit` | `DateTimeUnit` |

#### Returns

[DateTime](datetime.md)

___

### clone

▸ **clone**(): [DateTime](datetime.md)

#### Returns

[DateTime](datetime.md)

___

### daysInMonth

▸ **daysInMonth**(): `number`

#### Returns

`number`

___

### diff

▸ **diff**(`that`, `unit?`, `asFloat?`): `any`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `that` | [DateTime](datetime.md) | `undefined` |
| `unit?` | `DateTimeUnit` | `undefined` |
| `asFloat` | `boolean` | false |

#### Returns

`any`

___

### diffSecond

▸ **diffSecond**(`dt`): `number`

Diff:

#### Parameters

| Name | Type |
| :------ | :------ |
| `dt` | [DateTime](datetime.md) |

#### Returns

`number`

___

### endOf

▸ **endOf**(`unit`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `unit` | `DateTimeUnit` |

#### Returns

[DateTime](datetime.md)

___

### get

▸ **get**(`unit`): `number`

#### Parameters

| Name | Type |
| :------ | :------ |
| `unit` | `DateTimeUnit` |

#### Returns

`number`

___

### getDateObject

▸ **getDateObject**(): `Date`

#### Returns

`Date`

___

### getDateString

▸ **getDateString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

___

### getDay

▸ **getDay**(): `number`

Get day in UTC

#### Returns

`number`

___

### getHour

▸ **getHour**(): `number`

Get Hour in UTC

#### Returns

`number`

___

### getIsoString

▸ **getIsoString**(): `string`

#### Returns

`string`

___

### getIsoWeekday

▸ **getIsoWeekday**(): `number`

#### Returns

`number`

___

### getLocalPreciseString

▸ **getLocalPreciseString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

___

### getLocalString

▸ **getLocalString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

___

### getLocalStringShort

▸ **getLocalStringShort**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

___

### getMeridian

▸ **getMeridian**(): `string`

Get meridian (am/pm) in current locale

#### Returns

`string`

___

### getMillisecond

▸ **getMillisecond**(): `number`

Get milliseconds in UTC

#### Returns

`number`

___

### getMinute

▸ **getMinute**(): `number`

Get minute in UTC

#### Returns

`number`

___

### getMonth

▸ **getMonth**(): `number`

Get month in UTC

#### Returns

`number`

___

### getMonthName

▸ **getMonthName**(`type?`): `string`

Get month name in current locale

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |

#### Returns

`string`

___

### getPreciseString

▸ **getPreciseString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

___

### getPreciseTimestamp

▸ **getPreciseTimestamp**(): `number`

Returns unix timestamp with milliseconds precision

#### Returns

`number`

___

### getSecond

▸ **getSecond**(): `number`

Get second in UTC

#### Returns

`number`

___

### getString

▸ **getString**(`format?`, `local?`): `string`

Gets a formatted date time string

Format properties:

| Symbol | Description | Example |
| :------ | :------ | :------ |
| YY | Year in 2 digit format | 21 |
| YYYY | Year in full format | 2021 |
| M | Month digit not padded with zeros | 4 |
| MM | Month padded to 2 digits | 04 |
| MMM | Short locale aware month name | Dec |
| MMMM | Long locale aware month name | December |
| d | Weekday number | 5 |
| dd | Weekday locale aware narrow name | F |
| ddd | Weekday locale aware short name | Fri |
| dddd | Weekday locale aware short name | Friday |
| D | Day of month not padded | 9 |
| DD | Day of month padded to 2 digits | 09 |
| H | Hour in 24h format not padded | 7 |
| HH | Hour in 24h format padded to 2 digits | 07 |
| h | Hour in 12h format not padded | 7 |
| hh | Hour in 12h format padded to 2 digits | 07 |
| a | Meridian locale aware in lowercase | am |
| A | Meridian locale aware in uppercase | AM |
| m | Minute not padded | 5 |
| mm | Minute padded to 2 digits | 05 |
| s | Second not padded | 3 |
| ss | Second padded to 2 digits | 03 |
| SSS | Milliseconds padded to 3 digits | 245 |
| Z | Timezone offset from UTC with colon | +02:00 |
| ZZ | Timezone offset from UTC without colon | +02:00 |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `format` | `string` | `undefined` | format to convert to |
| `local` | `boolean` | false | if this is set to true, current timezone offset will be used to format date to local time |

#### Returns

`string`

___

### getTimeString

▸ **getTimeString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

___

### getTimezoneOffset

▸ **getTimezoneOffset**(`timeZone?`): `number`

Get timezone offset in minutes from UTC, returns the number of minutes from utc ex -120

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `timeZone?` | `string` | IANA string for timezone |

#### Returns

`number`

___

### getTimezoneOffsetString

▸ **getTimezoneOffsetString**(`withColon?`, `timeZone?`): `string`

Gets timezone offset string in hours from UTC, ex `+0200` or if withColon specified: `+02:00`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `withColon` | `boolean` | true |
| `timeZone?` | `string` | `undefined` |

#### Returns

`string`

___

### getUnixTimestamp

▸ **getUnixTimestamp**(): `number`

Returns unix timestamp with seconds precision

#### Returns

`number`

___

### getWeekday

▸ **getWeekday**(): `number`

Get weekday in UTC

#### Returns

`number`

___

### getWeekdayName

▸ **getWeekdayName**(`type?`): `string`

Get weekday name in current locale

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |

#### Returns

`string`

___

### getYear

▸ **getYear**(): `number`

Get year in UTC

#### Returns

`number`

___

### incSecond

▸ **incSecond**(): [DateTime](datetime.md)

Incrementation functions:

#### Returns

[DateTime](datetime.md)

___

### isAfter

▸ **isAfter**(`other`, `unit?`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `other` | [DateTime](datetime.md) |
| `unit?` | `DateTimeUnit` |

#### Returns

`boolean`

___

### isBefore

▸ **isBefore**(`that`, `unit?`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `that` | [DateTime](datetime.md) |
| `unit?` | `DateTimeUnit` |

#### Returns

`boolean`

___

### isBeforeDay

▸ **isBeforeDay**(`dt`): `boolean`

Comparison functions:

#### Parameters

| Name | Type |
| :------ | :------ |
| `dt` | [DateTime](datetime.md) |

#### Returns

`boolean`

___

### isBetween

▸ **isBetween**(`start`, `end`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | [DateTime](datetime.md) |
| `end` | [DateTime](datetime.md) |

#### Returns

`boolean`

___

### isSame

▸ **isSame**(`other`, `unit?`): `boolean`

Checks if date is the same or is {@link DateTimeUnit} of is the same

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `other` | [DateTime](datetime.md) |  |
| `unit?` | `DateTimeUnit` | {@link DateTimeUnit} |

#### Returns

`boolean`

___

### isSameDay

▸ **isSameDay**(`dt`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `dt` | [DateTime](datetime.md) |

#### Returns

`boolean`

___

### isSameMonth

▸ **isSameMonth**(`dt`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `dt` | [DateTime](datetime.md) |

#### Returns

`boolean`

___

### isSameOrAfter

▸ **isSameOrAfter**(`other`, `unit?`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `other` | [DateTime](datetime.md) |
| `unit?` | `DateTimeUnit` |

#### Returns

`boolean`

___

### isSameOrBefore

▸ **isSameOrBefore**(`that`, `unit?`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `that` | [DateTime](datetime.md) |
| `unit?` | `DateTimeUnit` |

#### Returns

`boolean`

___

### isValid

▸ **isValid**(): `boolean`

#### Returns

`boolean`

___

### set

▸ **set**(`unit`, `value`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `unit` | `DateTimeUnit` |
| `value` | `number` |

#### Returns

[DateTime](datetime.md)

___

### setPreciseTimestamp

▸ **setPreciseTimestamp**(`unix`): [DateTime](datetime.md)

Sets the actual value of date object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `unix` | `number` | timestamp in milliseconds |

#### Returns

[DateTime](datetime.md)

___

### setUnixTimestamp

▸ **setUnixTimestamp**(`stamp`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `stamp` | `number` |

#### Returns

[DateTime](datetime.md)

___

### startOf

▸ **startOf**(`unit?`, `isStartOf?`): [DateTime](datetime.md)

Sets date time to a start of specific period

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `unit?` | `DateTimeUnit` | `undefined` |  |
| `isStartOf` | `boolean` | true | if this is set to false, function will return the end of period |

#### Returns

[DateTime](datetime.md)

___

### stripDate

▸ **stripDate**(): `DateInterface`

#### Returns

`DateInterface`

___

### stripTime

▸ **stripTime**(): `TimeInterface`

#### Returns

`TimeInterface`

___

### subtract

▸ **subtract**(`number`, `string`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `number` | `number` |
| `string` | `any` |

#### Returns

[DateTime](datetime.md)

___

### toDate

▸ **toDate**(): `Date`

#### Returns

`Date`

___

### toISOString

▸ **toISOString**(): `string`

#### Returns

`string`

___

### toJSON

▸ **toJSON**(): `string`

#### Returns

`string`

___

### toString

▸ **toString**(): `string`

#### Returns

`string`

___

### valueOf

▸ **valueOf**(): `number`

#### Returns

`number`

___

### clearMonthNamesOverride

▸ `Static` **clearMonthNamesOverride**(): `void`

Clears overridden month names

#### Returns

`void`

___

### clearWeekNamesOverride

▸ `Static` **clearWeekNamesOverride**(): `void`

Clears overridden week names

#### Returns

`void`

___

### fromLocalPreciseString

▸ `Static` **fromLocalPreciseString**(`input`, `format?`): [DateTime](datetime.md)

TODO: fix this

#### Parameters

| Name | Type |
| :------ | :------ |
| `input` | `string` |
| `format` | `string` |

#### Returns

[DateTime](datetime.md)

___

### fromPreciseTimestamp

▸ `Static` **fromPreciseTimestamp**(`timestampMilliseconds`): [DateTime](datetime.md)

Special constructors:

#### Parameters

| Name | Type |
| :------ | :------ |
| `timestampMilliseconds` | `number` |

#### Returns

[DateTime](datetime.md)

___

### fromUnixTimestamp

▸ `Static` **fromUnixTimestamp**(`timestamp`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `timestamp` | `number` |

#### Returns

[DateTime](datetime.md)

___

### fromUtcPreciseString

▸ `Static` **fromUtcPreciseString**(`input`, `format?`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `input` | `string` |
| `format` | `string` |

#### Returns

[DateTime](datetime.md)

___

### fromUtcString

▸ `Static` **fromUtcString**(`input`, `format?`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `input` | `string` |
| `format` | `string` |

#### Returns

[DateTime](datetime.md)

___

### getMonthName

▸ `Static` **getMonthName**(`m`, `type?`): `string`

Get month name: 1 based (1 = January)

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `m` | `number` | `undefined` | month number (not index) |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |  |

#### Returns

`string`

___

### getMonthNames

▸ `Static` **getMonthNames**(`type?`, `locale?`): `string`[]

Get array of month names, keep it in cache

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |
| `locale` | `string` | `undefined` |

#### Returns

`string`[]

___

### getNowString

▸ `Static` **getNowString**(`format?`): `string`

Gets current date and time with specified format

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `format` | `string` | default `YYYY-MM-DD HH:mm:ss` |

#### Returns

`string`

___

### getPreciseNowString

▸ `Static` **getPreciseNowString**(): `string`

Gets formatted date and time with milliseconds precision in format `YYYY-MM-DD HH:mm:ss.SSS`

#### Returns

`string`

___

### getPreciseTimestamp

▸ `Static` **getPreciseTimestamp**(): `number`

Gets JS timestamp (milliseconds precision) of current date and time

#### Returns

`number`

___

### getUnixTimestamp

▸ `Static` **getUnixTimestamp**(): `number`

Gets unix timestamp (seconds precision) of current date and time

#### Returns

`number`

___

### getWeekName

▸ `Static` **getWeekName**(`m`, `type?`): `string`

Get iso week name: 1 based (1 = Monday)

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `m` | `number` | `undefined` | week number (not index) |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |  |

#### Returns

`string`

___

### getWeekNames

▸ `Static` **getWeekNames**(`type?`, `locale?`): `string`[]

Get array of week names, keep it in cache

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |
| `locale` | `string` | `undefined` |

#### Returns

`string`[]

___

### isValid

▸ `Static` **isValid**(`s`, `format?`, `strict?`): `boolean`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `s` | `string` | `undefined` |
| `format` | `string` | `undefined` |
| `strict` | `boolean` | false |

#### Returns

`boolean`

___

### monthDiff

▸ `Static` **monthDiff**(`a`, `b`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `a` | [DateTime](datetime.md) |
| `b` | [DateTime](datetime.md) |

#### Returns

`any`

___

### parseDate

▸ `Static` **parseDate**(`date`): `Date`

Parse datetime from a string into javascript Date object.

#### Parameters

| Name | Type |
| :------ | :------ |
| `date` | `string` |

#### Returns

`Date`

___

### setDefaultLocale

▸ `Static` **setDefaultLocale**(`locale`): `void`

Changes global locale clearing data such as month names

#### Parameters

| Name | Type |
| :------ | :------ |
| `locale` | `string` |

#### Returns

`void`

___

### setDefaultTimezone

▸ `Static` **setDefaultTimezone**(`timezone`): `void`

Override the default timezone globally if you wish to use different timezone across all application

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `timezone` | `string` | IANA timezone string |

#### Returns

`void`

___

### setMonthNamesOverride

▸ `Static` **setMonthNamesOverride**(`names`): `void`

Overrides global month names

#### Parameters

| Name | Type |
| :------ | :------ |
| `names` | `DateTimeMonthLocale`[] |

#### Returns

`void`

___

### setWeekNamesOverride

▸ `Static` **setWeekNamesOverride**(`names`): `void`

Overrides global week names

#### Parameters

| Name | Type |
| :------ | :------ |
| `names` | `DateTimeMonthLocale`[] |

#### Returns

`void`
