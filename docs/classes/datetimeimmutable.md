[datetime-next](../README.md) / [Exports](../modules.md) / DateTimeImmutable

# Class: DateTimeImmutable

This class is exactly the same as [DateTime](datetime.md)

## Hierarchy

- [DateTime](datetime.md)

  ↳ **DateTimeImmutable**

## Table of contents

### Constructors

- [constructor](datetimeimmutable.md#constructor)

### Properties

- [date](datetimeimmutable.md#date)
- [localOffset](datetimeimmutable.md#localoffset)
- [locale](datetimeimmutable.md#locale)
- [timeZone](datetimeimmutable.md#timezone)
- [DATE\_FORMAT](datetimeimmutable.md#date_format)
- [DATE\_TIME\_FORMAT](datetimeimmutable.md#date_time_format)
- [DATE\_TIME\_FORMAT\_SHORT](datetimeimmutable.md#date_time_format_short)
- [PRECISE\_DATE\_TIME\_FORMAT](datetimeimmutable.md#precise_date_time_format)
- [PRECISE\_TIME\_FORMAT](datetimeimmutable.md#precise_time_format)
- [TIME\_FORMAT](datetimeimmutable.md#time_format)
- [TIME\_FORMAT\_SHORT](datetimeimmutable.md#time_format_short)

### Accessors

- [dateString](datetimeimmutable.md#datestring)
- [day](datetimeimmutable.md#day)
- [hour](datetimeimmutable.md#hour)
- [isoString](datetimeimmutable.md#isostring)
- [isoWeekday](datetimeimmutable.md#isoweekday)
- [localString](datetimeimmutable.md#localstring)
- [localStringShort](datetimeimmutable.md#localstringshort)
- [millisecond](datetimeimmutable.md#millisecond)
- [minute](datetimeimmutable.md#minute)
- [month](datetimeimmutable.md#month)
- [monthName](datetimeimmutable.md#monthname)
- [preciseTimestamp](datetimeimmutable.md#precisetimestamp)
- [second](datetimeimmutable.md#second)
- [timeString](datetimeimmutable.md#timestring)
- [unixTimestamp](datetimeimmutable.md#unixtimestamp)
- [weekday](datetimeimmutable.md#weekday)
- [year](datetimeimmutable.md#year)

### DateTime Manipulation - Add Methods

- [addDay](datetimeimmutable.md#addday)
- [addHour](datetimeimmutable.md#addhour)
- [addMillisecond](datetimeimmutable.md#addmillisecond)
- [addMinute](datetimeimmutable.md#addminute)
- [addMonth](datetimeimmutable.md#addmonth)
- [addSecond](datetimeimmutable.md#addsecond)
- [addWeek](datetimeimmutable.md#addweek)
- [addYear](datetimeimmutable.md#addyear)

### DateTime Manipulation - Setters Methods

- [setWeekday](datetimeimmutable.md#setweekday)

### DateTime Manipulation - Special Setters Methods

- [setTimeTo](datetimeimmutable.md#settimeto)
- [setTimeToEndOfDay](datetimeimmutable.md#settimetoendofday)
- [setTimeToNow](datetimeimmutable.md#settimetonow)
- [setTimeToZero](datetimeimmutable.md#settimetozero)
- [setToEndOfMonth](datetimeimmutable.md#settoendofmonth)
- [setToEndOfWeek](datetimeimmutable.md#settoendofweek)
- [setToStartOfMonth](datetimeimmutable.md#settostartofmonth)
- [setToStartOfWeek](datetimeimmutable.md#settostartofweek)

### DateTime Manipulation - Subtract Methods

- [subtractDay](datetimeimmutable.md#subtractday)
- [subtractHour](datetimeimmutable.md#subtracthour)
- [subtractMillisecond](datetimeimmutable.md#subtractmillisecond)
- [subtractMinute](datetimeimmutable.md#subtractminute)
- [subtractMonth](datetimeimmutable.md#subtractmonth)
- [subtractSecond](datetimeimmutable.md#subtractsecond)
- [subtractWeek](datetimeimmutable.md#subtractweek)
- [subtractYear](datetimeimmutable.md#subtractyear)

### Other Methods

- [add](datetimeimmutable.md#add)
- [clone](datetimeimmutable.md#clone)
- [daysInMonth](datetimeimmutable.md#daysinmonth)
- [diff](datetimeimmutable.md#diff)
- [diffSecond](datetimeimmutable.md#diffsecond)
- [endOf](datetimeimmutable.md#endof)
- [get](datetimeimmutable.md#get)
- [getDateObject](datetimeimmutable.md#getdateobject)
- [getDateString](datetimeimmutable.md#getdatestring)
- [getDay](datetimeimmutable.md#getday)
- [getHour](datetimeimmutable.md#gethour)
- [getIsoString](datetimeimmutable.md#getisostring)
- [getIsoWeekday](datetimeimmutable.md#getisoweekday)
- [getLocalPreciseString](datetimeimmutable.md#getlocalprecisestring)
- [getLocalString](datetimeimmutable.md#getlocalstring)
- [getLocalStringShort](datetimeimmutable.md#getlocalstringshort)
- [getMeridian](datetimeimmutable.md#getmeridian)
- [getMillisecond](datetimeimmutable.md#getmillisecond)
- [getMinute](datetimeimmutable.md#getminute)
- [getMonth](datetimeimmutable.md#getmonth)
- [getMonthName](datetimeimmutable.md#getmonthname)
- [getPreciseString](datetimeimmutable.md#getprecisestring)
- [getPreciseTimestamp](datetimeimmutable.md#getprecisetimestamp)
- [getSecond](datetimeimmutable.md#getsecond)
- [getString](datetimeimmutable.md#getstring)
- [getTimeString](datetimeimmutable.md#gettimestring)
- [getTimezoneOffset](datetimeimmutable.md#gettimezoneoffset)
- [getTimezoneOffsetString](datetimeimmutable.md#gettimezoneoffsetstring)
- [getUnixTimestamp](datetimeimmutable.md#getunixtimestamp)
- [getWeekday](datetimeimmutable.md#getweekday)
- [getWeekdayName](datetimeimmutable.md#getweekdayname)
- [getYear](datetimeimmutable.md#getyear)
- [incSecond](datetimeimmutable.md#incsecond)
- [isAfter](datetimeimmutable.md#isafter)
- [isBefore](datetimeimmutable.md#isbefore)
- [isBeforeDay](datetimeimmutable.md#isbeforeday)
- [isBetween](datetimeimmutable.md#isbetween)
- [isSame](datetimeimmutable.md#issame)
- [isSameDay](datetimeimmutable.md#issameday)
- [isSameMonth](datetimeimmutable.md#issamemonth)
- [isSameOrAfter](datetimeimmutable.md#issameorafter)
- [isSameOrBefore](datetimeimmutable.md#issameorbefore)
- [isValid](datetimeimmutable.md#isvalid)
- [set](datetimeimmutable.md#set)
- [setDay](datetimeimmutable.md#setday)
- [setHour](datetimeimmutable.md#sethour)
- [setMillisecond](datetimeimmutable.md#setmillisecond)
- [setMinute](datetimeimmutable.md#setminute)
- [setMonth](datetimeimmutable.md#setmonth)
- [setPreciseTimestamp](datetimeimmutable.md#setprecisetimestamp)
- [setSecond](datetimeimmutable.md#setsecond)
- [setUnixTimestamp](datetimeimmutable.md#setunixtimestamp)
- [setYear](datetimeimmutable.md#setyear)
- [startOf](datetimeimmutable.md#startof)
- [stripDate](datetimeimmutable.md#stripdate)
- [stripTime](datetimeimmutable.md#striptime)
- [subtract](datetimeimmutable.md#subtract)
- [toDate](datetimeimmutable.md#todate)
- [toISOString](datetimeimmutable.md#toisostring)
- [toJSON](datetimeimmutable.md#tojson)
- [toString](datetimeimmutable.md#tostring)
- [valueOf](datetimeimmutable.md#valueof)
- [clearMonthNamesOverride](datetimeimmutable.md#clearmonthnamesoverride)
- [clearWeekNamesOverride](datetimeimmutable.md#clearweeknamesoverride)
- [fromLocalPreciseString](datetimeimmutable.md#fromlocalprecisestring)
- [fromPreciseTimestamp](datetimeimmutable.md#fromprecisetimestamp)
- [fromUnixTimestamp](datetimeimmutable.md#fromunixtimestamp)
- [fromUtcPreciseString](datetimeimmutable.md#fromutcprecisestring)
- [fromUtcString](datetimeimmutable.md#fromutcstring)
- [getMonthName](datetimeimmutable.md#getmonthname)
- [getMonthNames](datetimeimmutable.md#getmonthnames)
- [getNowString](datetimeimmutable.md#getnowstring)
- [getPreciseNowString](datetimeimmutable.md#getprecisenowstring)
- [getPreciseTimestamp](datetimeimmutable.md#getprecisetimestamp)
- [getUnixTimestamp](datetimeimmutable.md#getunixtimestamp)
- [getWeekName](datetimeimmutable.md#getweekname)
- [getWeekNames](datetimeimmutable.md#getweeknames)
- [isValid](datetimeimmutable.md#isvalid)
- [monthDiff](datetimeimmutable.md#monthdiff)
- [parseDate](datetimeimmutable.md#parsedate)
- [setDefaultLocale](datetimeimmutable.md#setdefaultlocale)
- [setDefaultTimezone](datetimeimmutable.md#setdefaulttimezone)
- [setMonthNamesOverride](datetimeimmutable.md#setmonthnamesoverride)
- [setWeekNamesOverride](datetimeimmutable.md#setweeknamesoverride)

## Constructors

### constructor

• **new DateTimeImmutable**(`input?`)

Constructs a `DateTime` object based on provided data, input options:

* `number` - milliseconds from unix epoch
* `string` - ISO datetime format: `2020-09-23T17:23:11+04:30` also accepts partial data (only year is required)
* `Date` - Javascript `Date` object
* `DateTime` - another instance of `DateTime` (clone will be performed)

#### Parameters

| Name | Type |
| :------ | :------ |
| `input?` | `string` \| `number` \| [DateTime](datetime.md) \| `Date` |

#### Inherited from

[DateTime](datetime.md).[constructor](datetime.md#constructor)

## Properties

### date

• **date**: `Date`

#### Inherited from

[DateTime](datetime.md).[date](datetime.md#date)

___

### localOffset

• **localOffset**: `number` = 0

#### Inherited from

[DateTime](datetime.md).[localOffset](datetime.md#localoffset)

___

### locale

• **locale**: `string`

#### Inherited from

[DateTime](datetime.md).[locale](datetime.md#locale)

___

### timeZone

• **timeZone**: `string`

#### Inherited from

[DateTime](datetime.md).[timeZone](datetime.md#timezone)

___

### DATE\_FORMAT

▪ `Static` **DATE\_FORMAT**: `string` = 'YYYY-MM-DD'

#### Inherited from

[DateTime](datetime.md).[DATE_FORMAT](datetime.md#date_format)

___

### DATE\_TIME\_FORMAT

▪ `Static` **DATE\_TIME\_FORMAT**: `string`

#### Inherited from

[DateTime](datetime.md).[DATE_TIME_FORMAT](datetime.md#date_time_format)

___

### DATE\_TIME\_FORMAT\_SHORT

▪ `Static` **DATE\_TIME\_FORMAT\_SHORT**: `string`

#### Inherited from

[DateTime](datetime.md).[DATE_TIME_FORMAT_SHORT](datetime.md#date_time_format_short)

___

### PRECISE\_DATE\_TIME\_FORMAT

▪ `Static` **PRECISE\_DATE\_TIME\_FORMAT**: `string`

#### Inherited from

[DateTime](datetime.md).[PRECISE_DATE_TIME_FORMAT](datetime.md#precise_date_time_format)

___

### PRECISE\_TIME\_FORMAT

▪ `Static` **PRECISE\_TIME\_FORMAT**: `string` = 'HH:mm:ss.SSS'

#### Inherited from

[DateTime](datetime.md).[PRECISE_TIME_FORMAT](datetime.md#precise_time_format)

___

### TIME\_FORMAT

▪ `Static` **TIME\_FORMAT**: `string` = 'HH:mm:ss'

#### Inherited from

[DateTime](datetime.md).[TIME_FORMAT](datetime.md#time_format)

___

### TIME\_FORMAT\_SHORT

▪ `Static` **TIME\_FORMAT\_SHORT**: `string` = 'HH:mm'

#### Inherited from

[DateTime](datetime.md).[TIME_FORMAT_SHORT](datetime.md#time_format_short)

## Accessors

### dateString

• `get` **dateString**(): `string`

#### Returns

`string`

___

### day

• `get` **day**(): `number`

#### Returns

`number`

___

### hour

• `get` **hour**(): `number`

#### Returns

`number`

___

### isoString

• `get` **isoString**(): `string`

#### Returns

`string`

___

### isoWeekday

• `get` **isoWeekday**(): `number`

#### Returns

`number`

___

### localString

• `get` **localString**(): `string`

#### Returns

`string`

___

### localStringShort

• `get` **localStringShort**(): `string`

#### Returns

`string`

___

### millisecond

• `get` **millisecond**(): `number`

#### Returns

`number`

___

### minute

• `get` **minute**(): `number`

#### Returns

`number`

___

### month

• `get` **month**(): `number`

#### Returns

`number`

___

### monthName

• `get` **monthName**(): `string`

#### Returns

`string`

___

### preciseTimestamp

• `get` **preciseTimestamp**(): `number`

#### Returns

`number`

___

### second

• `get` **second**(): `number`

#### Returns

`number`

___

### timeString

• `get` **timeString**(): `string`

#### Returns

`string`

___

### unixTimestamp

• `get` **unixTimestamp**(): `number`

#### Returns

`number`

___

### weekday

• `get` **weekday**(): `number`

#### Returns

`number`

___

### year

• `get` **year**(): `number`

#### Returns

`number`

## DateTime Manipulation - Add Methods

### addDay

▸ **addDay**(`value`): [DateTime](datetime.md)

Add certain amount of days to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of days |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[addDay](datetime.md#addday)

___

### addHour

▸ **addHour**(`value`): [DateTime](datetime.md)

Add certain amount of hours to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of hours |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[addHour](datetime.md#addhour)

___

### addMillisecond

▸ **addMillisecond**(`v`): [DateTime](datetime.md)

Add certain amount of milliseconds to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `v` | `number` | number of milliseconds |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[addMillisecond](datetime.md#addmillisecond)

___

### addMinute

▸ **addMinute**(`value`): [DateTime](datetime.md)

Add certain amount of minutes to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of minutes |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[addMinute](datetime.md#addminute)

___

### addMonth

▸ **addMonth**(`value`): [DateTime](datetime.md)

Add certain amount of months to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of months |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[addMonth](datetime.md#addmonth)

___

### addSecond

▸ **addSecond**(`value`): [DateTime](datetime.md)

Add certain amount of seconds to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of seconds |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[addSecond](datetime.md#addsecond)

___

### addWeek

▸ **addWeek**(`value`): [DateTime](datetime.md)

Add certain amount of weeks to datetime, this is equivalent of days * 7

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of weeks |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[addWeek](datetime.md#addweek)

___

### addYear

▸ **addYear**(`value`): [DateTime](datetime.md)

Add certain amount of years to datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of years |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[addYear](datetime.md#addyear)

___

## DateTime Manipulation - Setters Methods

### setWeekday

▸ **setWeekday**(`week`): [DateTime](datetime.md)

Sets the UTC weekday

#### Parameters

| Name | Type |
| :------ | :------ |
| `week` | `number` |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setWeekday](datetime.md#setweekday)

___

## DateTime Manipulation - Special Setters Methods

### setTimeTo

▸ **setTimeTo**(`zero?`): [DateTime](datetime.md)

Sets the time to either 00:00:00.000 or 23:59:59.999

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `zero` | `boolean` | false |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setTimeTo](datetime.md#settimeto)

___

### setTimeToEndOfDay

▸ **setTimeToEndOfDay**(): [DateTime](datetime.md)

Sets the time to 23:59:59.999

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setTimeToEndOfDay](datetime.md#settimetoendofday)

___

### setTimeToNow

▸ **setTimeToNow**(): [DateTime](datetime.md)

Sets the time to current time without changing date

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setTimeToNow](datetime.md#settimetonow)

___

### setTimeToZero

▸ **setTimeToZero**(): [DateTime](datetime.md)

Sets the time to 00:00:00.000

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setTimeToZero](datetime.md#settimetozero)

___

### setToEndOfMonth

▸ **setToEndOfMonth**(): [DateTime](datetime.md)

Sets the time and date to UTC start of month

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setToEndOfMonth](datetime.md#settoendofmonth)

___

### setToEndOfWeek

▸ **setToEndOfWeek**(): [DateTime](datetime.md)

Sets the time and date to UTC end of week

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setToEndOfWeek](datetime.md#settoendofweek)

___

### setToStartOfMonth

▸ **setToStartOfMonth**(): [DateTime](datetime.md)

Sets the time and date to UTC start of month

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setToStartOfMonth](datetime.md#settostartofmonth)

___

### setToStartOfWeek

▸ **setToStartOfWeek**(): [DateTime](datetime.md)

Sets the time and date to UTC start of week

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setToStartOfWeek](datetime.md#settostartofweek)

___

## DateTime Manipulation - Subtract Methods

### subtractDay

▸ **subtractDay**(`value`): [DateTime](datetime.md)

Subtract certain amount of days from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of days |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtractDay](datetime.md#subtractday)

___

### subtractHour

▸ **subtractHour**(`value`): [DateTime](datetime.md)

Subtract certain amount of hours from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of hours |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtractHour](datetime.md#subtracthour)

___

### subtractMillisecond

▸ **subtractMillisecond**(`value`): [DateTime](datetime.md)

Subtract certain amount of milliseconds from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of milliseconds |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtractMillisecond](datetime.md#subtractmillisecond)

___

### subtractMinute

▸ **subtractMinute**(`value`): [DateTime](datetime.md)

Subtract certain amount of minutes from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of minutes |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtractMinute](datetime.md#subtractminute)

___

### subtractMonth

▸ **subtractMonth**(`value`): [DateTime](datetime.md)

Subtract certain amount of months from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of months |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtractMonth](datetime.md#subtractmonth)

___

### subtractSecond

▸ **subtractSecond**(`value`): [DateTime](datetime.md)

Subtract certain amount of seconds from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of seconds |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtractSecond](datetime.md#subtractsecond)

___

### subtractWeek

▸ **subtractWeek**(`value`): [DateTime](datetime.md)

Subtract certain amount of weeks from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of weeks |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtractWeek](datetime.md#subtractweek)

___

### subtractYear

▸ **subtractYear**(`value`): [DateTime](datetime.md)

Subtract certain amount of years from datetime

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `number` | number of years |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtractYear](datetime.md#subtractyear)

___

## Other Methods

### add

▸ **add**(`value`, `unit`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `value` | `number` |
| `unit` | `DateTimeUnit` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[add](datetime.md#add)

___

### clone

▸ **clone**(): [DateTime](datetime.md)

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[clone](datetime.md#clone)

___

### daysInMonth

▸ **daysInMonth**(): `number`

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[daysInMonth](datetime.md#daysinmonth)

___

### diff

▸ **diff**(`that`, `unit?`, `asFloat?`): `any`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `that` | [DateTime](datetime.md) | `undefined` |
| `unit?` | `DateTimeUnit` | `undefined` |
| `asFloat` | `boolean` | false |

#### Returns

`any`

#### Inherited from

[DateTime](datetime.md).[diff](datetime.md#diff)

___

### diffSecond

▸ **diffSecond**(`dt`): `number`

Diff:

#### Parameters

| Name | Type |
| :------ | :------ |
| `dt` | [DateTime](datetime.md) |

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[diffSecond](datetime.md#diffsecond)

___

### endOf

▸ **endOf**(`unit`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `unit` | `DateTimeUnit` |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[endOf](datetime.md#endof)

___

### get

▸ **get**(`unit`): `number`

#### Parameters

| Name | Type |
| :------ | :------ |
| `unit` | `DateTimeUnit` |

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[get](datetime.md#get)

___

### getDateObject

▸ **getDateObject**(): `Date`

#### Returns

`Date`

#### Inherited from

[DateTime](datetime.md).[getDateObject](datetime.md#getdateobject)

___

### getDateString

▸ **getDateString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getDateString](datetime.md#getdatestring)

___

### getDay

▸ **getDay**(): `number`

Get day in UTC

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getDay](datetime.md#getday)

___

### getHour

▸ **getHour**(): `number`

Get Hour in UTC

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getHour](datetime.md#gethour)

___

### getIsoString

▸ **getIsoString**(): `string`

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getIsoString](datetime.md#getisostring)

___

### getIsoWeekday

▸ **getIsoWeekday**(): `number`

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getIsoWeekday](datetime.md#getisoweekday)

___

### getLocalPreciseString

▸ **getLocalPreciseString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getLocalPreciseString](datetime.md#getlocalprecisestring)

___

### getLocalString

▸ **getLocalString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getLocalString](datetime.md#getlocalstring)

___

### getLocalStringShort

▸ **getLocalStringShort**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getLocalStringShort](datetime.md#getlocalstringshort)

___

### getMeridian

▸ **getMeridian**(): `string`

Get meridian (am/pm) in current locale

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getMeridian](datetime.md#getmeridian)

___

### getMillisecond

▸ **getMillisecond**(): `number`

Get milliseconds in UTC

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getMillisecond](datetime.md#getmillisecond)

___

### getMinute

▸ **getMinute**(): `number`

Get minute in UTC

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getMinute](datetime.md#getminute)

___

### getMonth

▸ **getMonth**(): `number`

Get month in UTC

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getMonth](datetime.md#getmonth)

___

### getMonthName

▸ **getMonthName**(`type?`): `string`

Get month name in current locale

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getMonthName](datetime.md#getmonthname)

___

### getPreciseString

▸ **getPreciseString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getPreciseString](datetime.md#getprecisestring)

___

### getPreciseTimestamp

▸ **getPreciseTimestamp**(): `number`

Returns unix timestamp with milliseconds precision

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getPreciseTimestamp](datetime.md#getprecisetimestamp)

___

### getSecond

▸ **getSecond**(): `number`

Get second in UTC

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getSecond](datetime.md#getsecond)

___

### getString

▸ **getString**(`format?`, `local?`): `string`

Gets a formatted date time string

Format properties:

| Symbol | Description | Example |
| :------ | :------ | :------ |
| YY | Year in 2 digit format | 21 |
| YYYY | Year in full format | 2021 |
| M | Month digit not padded with zeros | 4 |
| MM | Month padded to 2 digits | 04 |
| MMM | Short locale aware month name | Dec |
| MMMM | Long locale aware month name | December |
| d | Weekday number | 5 |
| dd | Weekday locale aware narrow name | F |
| ddd | Weekday locale aware short name | Fri |
| dddd | Weekday locale aware short name | Friday |
| D | Day of month not padded | 9 |
| DD | Day of month padded to 2 digits | 09 |
| H | Hour in 24h format not padded | 7 |
| HH | Hour in 24h format padded to 2 digits | 07 |
| h | Hour in 12h format not padded | 7 |
| hh | Hour in 12h format padded to 2 digits | 07 |
| a | Meridian locale aware in lowercase | am |
| A | Meridian locale aware in uppercase | AM |
| m | Minute not padded | 5 |
| mm | Minute padded to 2 digits | 05 |
| s | Second not padded | 3 |
| ss | Second padded to 2 digits | 03 |
| SSS | Milliseconds padded to 3 digits | 245 |
| Z | Timezone offset from UTC with colon | +02:00 |
| ZZ | Timezone offset from UTC without colon | +02:00 |

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `format` | `string` | `undefined` | format to convert to |
| `local` | `boolean` | false | if this is set to true, current timezone offset will be used to format date to local time |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getString](datetime.md#getstring)

___

### getTimeString

▸ **getTimeString**(`format?`): `string`

#### Parameters

| Name | Type |
| :------ | :------ |
| `format` | `string` |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getTimeString](datetime.md#gettimestring)

___

### getTimezoneOffset

▸ **getTimezoneOffset**(`timeZone?`): `number`

Get timezone offset in minutes from UTC, returns the number of minutes from utc ex -120

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `timeZone?` | `string` | IANA string for timezone |

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getTimezoneOffset](datetime.md#gettimezoneoffset)

___

### getTimezoneOffsetString

▸ **getTimezoneOffsetString**(`withColon?`, `timeZone?`): `string`

Gets timezone offset string in hours from UTC, ex `+0200` or if withColon specified: `+02:00`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `withColon` | `boolean` | true |
| `timeZone?` | `string` | `undefined` |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getTimezoneOffsetString](datetime.md#gettimezoneoffsetstring)

___

### getUnixTimestamp

▸ **getUnixTimestamp**(): `number`

Returns unix timestamp with seconds precision

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getUnixTimestamp](datetime.md#getunixtimestamp)

___

### getWeekday

▸ **getWeekday**(): `number`

Get weekday in UTC

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getWeekday](datetime.md#getweekday)

___

### getWeekdayName

▸ **getWeekdayName**(`type?`): `string`

Get weekday name in current locale

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getWeekdayName](datetime.md#getweekdayname)

___

### getYear

▸ **getYear**(): `number`

Get year in UTC

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getYear](datetime.md#getyear)

___

### incSecond

▸ **incSecond**(): [DateTime](datetime.md)

Incrementation functions:

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[incSecond](datetime.md#incsecond)

___

### isAfter

▸ **isAfter**(`other`, `unit?`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `other` | [DateTime](datetime.md) |
| `unit?` | `DateTimeUnit` |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isAfter](datetime.md#isafter)

___

### isBefore

▸ **isBefore**(`that`, `unit?`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `that` | [DateTime](datetime.md) |
| `unit?` | `DateTimeUnit` |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isBefore](datetime.md#isbefore)

___

### isBeforeDay

▸ **isBeforeDay**(`dt`): `boolean`

Comparison functions:

#### Parameters

| Name | Type |
| :------ | :------ |
| `dt` | [DateTime](datetime.md) |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isBeforeDay](datetime.md#isbeforeday)

___

### isBetween

▸ **isBetween**(`start`, `end`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `start` | [DateTime](datetime.md) |
| `end` | [DateTime](datetime.md) |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isBetween](datetime.md#isbetween)

___

### isSame

▸ **isSame**(`other`, `unit?`): `boolean`

Checks if date is the same or is {@link DateTimeUnit} of is the same

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `other` | [DateTime](datetime.md) |  |
| `unit?` | `DateTimeUnit` | {@link DateTimeUnit} |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isSame](datetime.md#issame)

___

### isSameDay

▸ **isSameDay**(`dt`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `dt` | [DateTime](datetime.md) |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isSameDay](datetime.md#issameday)

___

### isSameMonth

▸ **isSameMonth**(`dt`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `dt` | [DateTime](datetime.md) |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isSameMonth](datetime.md#issamemonth)

___

### isSameOrAfter

▸ **isSameOrAfter**(`other`, `unit?`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `other` | [DateTime](datetime.md) |
| `unit?` | `DateTimeUnit` |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isSameOrAfter](datetime.md#issameorafter)

___

### isSameOrBefore

▸ **isSameOrBefore**(`that`, `unit?`): `boolean`

#### Parameters

| Name | Type |
| :------ | :------ |
| `that` | [DateTime](datetime.md) |
| `unit?` | `DateTimeUnit` |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isSameOrBefore](datetime.md#issameorbefore)

___

### isValid

▸ **isValid**(): `boolean`

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isValid](datetime.md#isvalid)

___

### set

▸ **set**(`unit`, `value`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `unit` | `DateTimeUnit` |
| `value` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[set](datetime.md#set)

___

### setDay

▸ **setDay**(`d`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `d` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[setDay](datetime.md#setday)

___

### setHour

▸ **setHour**(`h`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `h` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[setHour](datetime.md#sethour)

___

### setMillisecond

▸ **setMillisecond**(`ms`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `ms` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[setMillisecond](datetime.md#setmillisecond)

___

### setMinute

▸ **setMinute**(`m`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `m` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[setMinute](datetime.md#setminute)

___

### setMonth

▸ **setMonth**(`m`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `m` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[setMonth](datetime.md#setmonth)

___

### setPreciseTimestamp

▸ **setPreciseTimestamp**(`unix`): [DateTime](datetime.md)

Sets the actual value of date object

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `unix` | `number` | timestamp in milliseconds |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[setPreciseTimestamp](datetime.md#setprecisetimestamp)

___

### setSecond

▸ **setSecond**(`s`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `s` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[setSecond](datetime.md#setsecond)

___

### setUnixTimestamp

▸ **setUnixTimestamp**(`stamp`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `stamp` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[setUnixTimestamp](datetime.md#setunixtimestamp)

___

### setYear

▸ **setYear**(`y`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `y` | `number` |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[setYear](datetime.md#setyear)

___

### startOf

▸ **startOf**(`unit?`, `isStartOf?`): [DateTime](datetime.md)

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `unit?` | `DateTimeUnit` | `undefined` |
| `isStartOf` | `boolean` | true |

#### Returns

[DateTime](datetime.md)

#### Overrides

[DateTime](datetime.md).[startOf](datetime.md#startof)

___

### stripDate

▸ **stripDate**(): `DateInterface`

#### Returns

`DateInterface`

#### Inherited from

[DateTime](datetime.md).[stripDate](datetime.md#stripdate)

___

### stripTime

▸ **stripTime**(): `TimeInterface`

#### Returns

`TimeInterface`

#### Inherited from

[DateTime](datetime.md).[stripTime](datetime.md#striptime)

___

### subtract

▸ **subtract**(`number`, `string`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `number` | `number` |
| `string` | `any` |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[subtract](datetime.md#subtract)

___

### toDate

▸ **toDate**(): `Date`

#### Returns

`Date`

#### Inherited from

[DateTime](datetime.md).[toDate](datetime.md#todate)

___

### toISOString

▸ **toISOString**(): `string`

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[toISOString](datetime.md#toisostring)

___

### toJSON

▸ **toJSON**(): `string`

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[toJSON](datetime.md#tojson)

___

### toString

▸ **toString**(): `string`

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[toString](datetime.md#tostring)

___

### valueOf

▸ **valueOf**(): `number`

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[valueOf](datetime.md#valueof)

___

### clearMonthNamesOverride

▸ `Static` **clearMonthNamesOverride**(): `void`

Clears overridden month names

#### Returns

`void`

#### Inherited from

[DateTime](datetime.md).[clearMonthNamesOverride](datetime.md#clearmonthnamesoverride)

___

### clearWeekNamesOverride

▸ `Static` **clearWeekNamesOverride**(): `void`

Clears overridden week names

#### Returns

`void`

#### Inherited from

[DateTime](datetime.md).[clearWeekNamesOverride](datetime.md#clearweeknamesoverride)

___

### fromLocalPreciseString

▸ `Static` **fromLocalPreciseString**(`input`, `format?`): [DateTime](datetime.md)

TODO: fix this

#### Parameters

| Name | Type |
| :------ | :------ |
| `input` | `string` |
| `format` | `string` |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[fromLocalPreciseString](datetime.md#fromlocalprecisestring)

___

### fromPreciseTimestamp

▸ `Static` **fromPreciseTimestamp**(`timestampMilliseconds`): [DateTime](datetime.md)

Special constructors:

#### Parameters

| Name | Type |
| :------ | :------ |
| `timestampMilliseconds` | `number` |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[fromPreciseTimestamp](datetime.md#fromprecisetimestamp)

___

### fromUnixTimestamp

▸ `Static` **fromUnixTimestamp**(`timestamp`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `timestamp` | `number` |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[fromUnixTimestamp](datetime.md#fromunixtimestamp)

___

### fromUtcPreciseString

▸ `Static` **fromUtcPreciseString**(`input`, `format?`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `input` | `string` |
| `format` | `string` |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[fromUtcPreciseString](datetime.md#fromutcprecisestring)

___

### fromUtcString

▸ `Static` **fromUtcString**(`input`, `format?`): [DateTime](datetime.md)

#### Parameters

| Name | Type |
| :------ | :------ |
| `input` | `string` |
| `format` | `string` |

#### Returns

[DateTime](datetime.md)

#### Inherited from

[DateTime](datetime.md).[fromUtcString](datetime.md#fromutcstring)

___

### getMonthName

▸ `Static` **getMonthName**(`m`, `type?`): `string`

Get month name: 1 based (1 = January)

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `m` | `number` | `undefined` | month number (not index) |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |  |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getMonthName](datetime.md#getmonthname)

___

### getMonthNames

▸ `Static` **getMonthNames**(`type?`, `locale?`): `string`[]

Get array of month names, keep it in cache

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |
| `locale` | `string` | `undefined` |

#### Returns

`string`[]

#### Inherited from

[DateTime](datetime.md).[getMonthNames](datetime.md#getmonthnames)

___

### getNowString

▸ `Static` **getNowString**(`format?`): `string`

Gets current date and time with specified format

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `format` | `string` | default `YYYY-MM-DD HH:mm:ss` |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getNowString](datetime.md#getnowstring)

___

### getPreciseNowString

▸ `Static` **getPreciseNowString**(): `string`

Gets formatted date and time with milliseconds precision in format `YYYY-MM-DD HH:mm:ss.SSS`

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getPreciseNowString](datetime.md#getprecisenowstring)

___

### getPreciseTimestamp

▸ `Static` **getPreciseTimestamp**(): `number`

Gets JS timestamp (milliseconds precision) of current date and time

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getPreciseTimestamp](datetime.md#getprecisetimestamp)

___

### getUnixTimestamp

▸ `Static` **getUnixTimestamp**(): `number`

Gets unix timestamp (seconds precision) of current date and time

#### Returns

`number`

#### Inherited from

[DateTime](datetime.md).[getUnixTimestamp](datetime.md#getunixtimestamp)

___

### getWeekName

▸ `Static` **getWeekName**(`m`, `type?`): `string`

Get iso week name: 1 based (1 = Monday)

#### Parameters

| Name | Type | Default value | Description |
| :------ | :------ | :------ | :------ |
| `m` | `number` | `undefined` | week number (not index) |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |  |

#### Returns

`string`

#### Inherited from

[DateTime](datetime.md).[getWeekName](datetime.md#getweekname)

___

### getWeekNames

▸ `Static` **getWeekNames**(`type?`, `locale?`): `string`[]

Get array of week names, keep it in cache

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `type` | ``"short"`` \| ``"long"`` \| ``"narrow"`` | 'long' |
| `locale` | `string` | `undefined` |

#### Returns

`string`[]

#### Inherited from

[DateTime](datetime.md).[getWeekNames](datetime.md#getweeknames)

___

### isValid

▸ `Static` **isValid**(`s`, `format?`, `strict?`): `boolean`

#### Parameters

| Name | Type | Default value |
| :------ | :------ | :------ |
| `s` | `string` | `undefined` |
| `format` | `string` | `undefined` |
| `strict` | `boolean` | false |

#### Returns

`boolean`

#### Inherited from

[DateTime](datetime.md).[isValid](datetime.md#isvalid)

___

### monthDiff

▸ `Static` **monthDiff**(`a`, `b`): `any`

#### Parameters

| Name | Type |
| :------ | :------ |
| `a` | [DateTime](datetime.md) |
| `b` | [DateTime](datetime.md) |

#### Returns

`any`

#### Inherited from

[DateTime](datetime.md).[monthDiff](datetime.md#monthdiff)

___

### parseDate

▸ `Static` **parseDate**(`date`): `Date`

Parse datetime from a string into javascript Date object.

#### Parameters

| Name | Type |
| :------ | :------ |
| `date` | `string` |

#### Returns

`Date`

#### Inherited from

[DateTime](datetime.md).[parseDate](datetime.md#parsedate)

___

### setDefaultLocale

▸ `Static` **setDefaultLocale**(`locale`): `void`

Changes global locale clearing data such as month names

#### Parameters

| Name | Type |
| :------ | :------ |
| `locale` | `string` |

#### Returns

`void`

#### Inherited from

[DateTime](datetime.md).[setDefaultLocale](datetime.md#setdefaultlocale)

___

### setDefaultTimezone

▸ `Static` **setDefaultTimezone**(`timezone`): `void`

Override the default timezone globally if you wish to use different timezone across all application

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `timezone` | `string` | IANA timezone string |

#### Returns

`void`

#### Inherited from

[DateTime](datetime.md).[setDefaultTimezone](datetime.md#setdefaulttimezone)

___

### setMonthNamesOverride

▸ `Static` **setMonthNamesOverride**(`names`): `void`

Overrides global month names

#### Parameters

| Name | Type |
| :------ | :------ |
| `names` | `DateTimeMonthLocale`[] |

#### Returns

`void`

#### Inherited from

[DateTime](datetime.md).[setMonthNamesOverride](datetime.md#setmonthnamesoverride)

___

### setWeekNamesOverride

▸ `Static` **setWeekNamesOverride**(`names`): `void`

Overrides global week names

#### Parameters

| Name | Type |
| :------ | :------ |
| `names` | `DateTimeMonthLocale`[] |

#### Returns

`void`

#### Inherited from

[DateTime](datetime.md).[setWeekNamesOverride](datetime.md#setweeknamesoverride)
