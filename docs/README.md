datetime-next / [Exports](modules.md)

# DateTime Next

---
Next gen Date and Time manipulation library designed with performance in mind, 
it is **extremely lightweight**. It is about 3.5Kb minified and gzipped.

**Does not use any static data such as localization files**, everything comes from built in 
[Intl](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl) object.
However, this can be overridden if you provide your own Month names.

**`DateTime` is designed to only operate on UTC date and time.** 
It has however support for formatting date and time in user specified locale and timezone.

`DateTime` is specifically built as **Mutable** object, if you wish to use immutable object use
`DateTimeImmutable` instead. [Read more about this choice here](https://gitlab.com/tde-npm-packages/datetime-next/-/blob/master/Immutability.md) 

**Zero dependencies.**

![npm](https://img.shields.io/npm/v/datetime-next)

**WARNING:** API is not yet final

## Requirements

Technically it should have no requirements, For node recommended version 8+.
For browser, it's the existence of `Intl` object.

---

## Docs

[Read the docs here](https://gitlab.com/tde-npm-packages/datetime-next/-/blob/master/docs/modules.md)

---

## Installation

For yarn:

```
yarn add datetime-next
```

For npm:

```
npm i datetime-next
```

---

For browser just include the script or import it via ES6 import statement.

## Some examples

---

Imports:

```ts
// ES6 JS/Typescript style
import { DateTime } from 'datetime-next';
// OR
import { DateTime } from 'datetime-next/DateTime';

// require

const { DateTime } = require('datetime-next');
```

Basic usage:

```ts
const dt = new DateTime(); // Current datetime in UTC
dt.addHour(1); // Add one hour
dt.getString('YYYY-MM-DD'); // format date ex `2021-06-21`
dt.getString('HH:mm:ss'); // format time `15:25:41`
```

Format properties:

| Symbol | Description | Example |
| :------ | :------ | :------ |
| YY | Year in 2 digit format | 21 |
| YYYY | Year in full format | 2021 |
| M | Month digit not padded with zeros | 4 |
| MM | Month padded to 2 digits | 04 |
| MMM | Short locale aware month name | Dec |
| MMMM | Long locale aware month name | December |
| d | Weekday number | 5 |
| dd | Weekday locale aware narrow name | F |
| ddd | Weekday locale aware short name | Fri |
| dddd | Weekday locale aware short name | Friday |
| D | Day of month not padded | 9 |
| DD | Day of month padded to 2 digits | 09 |
| H | Hour in 24h format not padded | 7 |
| HH | Hour in 24h format padded to 2 digits | 07 |
| h | Hour in 12h format not padded | 7 |
| hh | Hour in 12h format padded to 2 digits | 07 |
| a | Meridian locale aware in lowercase | am |
| A | Meridian locale aware in uppercase | AM |
| m | Minute not padded | 5 |
| mm | Minute padded to 2 digits | 05 |
| s | Second not padded | 3 |
| ss | Second padded to 2 digits | 03 |
| SSS | Milliseconds padded to 3 digits | 245 |
| Z | Timezone offset from UTC with colon | +02:00 |
| ZZ | Timezone offset from UTC without colon | +02:00 |
